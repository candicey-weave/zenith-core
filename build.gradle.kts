plugins {
    kotlin("jvm") version "1.9.23"
    `java-library`
    `maven-publish`
    id("com.gitlab.candicey.stellar") version "0.2.0"
}

val projectName: String by project
val projectGroup: String by project
val projectVersion: String by project
val gitlabProjectId: String by project

stellar {
    relocate {
        relocateAsm()

        addClassReplacement(
            "com/gitlab/candicey/zenithcore",
            "com/gitlab/candicey/zenithcore_v${projectVersion.replace('.', '_')}"
        )

        addStringReplacement(
            "@@ZENITH_CORE_VERSION@@",
            projectVersion
        )
    }
}

allprojects {
    group = projectGroup
    version = projectVersion

    repositories {
        mavenCentral()
        mavenLocal()
        maven("https://jitpack.io")
        maven("https://repo.spongepowered.org/maven")
        maven("https://repo.weavemc.dev/releases")

        maven {
            url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = findProperty("gitLabPrivateToken") as String?
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

dependencies {
    testImplementation(kotlin("test"))

    implementation(libs.log4j)
    implementation(libs.gson)
    implementation(libs.asm)
    implementation(libs.weaveLoader)
    implementation(libs.weaveCommon)
    implementation(libs.weaveInternals)
    implementation(libs.kotlinReflect)
    implementation(libs.kotlinxSerialization)
    compileOnly(libs.mixin)
}

kotlin {
    jvmToolchain(8)
}

java {
    withSourcesJar()
    withJavadocJar()
}

tasks.test {
    useJUnitPlatform()
}

publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])

            val libsDirectory = File(project.projectDir, "build/libs")
            val file = libsDirectory.resolve("${project.name}-${project.version}-relocated.jar")
            artifact(file) {
                classifier = "relocated"
            }
        }
    }

    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = findProperty("gitLabPrivateToken") as String?
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}
