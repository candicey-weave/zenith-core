package com.gitlab.candicey.zenithcore.versioned.lunar

import com.gitlab.candicey.zenithcore.extension.parse
import com.gitlab.candicey.zenithcore.versioned.lunar.helper.LunarClassHelper
import org.objectweb.asm.tree.ClassNode

val lunarClasses: List<ClassNode> by lazy { LunarClassHelper.classBytes["lunar.jar"]!!.entries.map { it.value.parse().second } }