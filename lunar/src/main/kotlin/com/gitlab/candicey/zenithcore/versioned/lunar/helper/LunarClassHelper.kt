package com.gitlab.candicey.zenithcore.versioned.lunar.helper

import com.gitlab.candicey.zenithcore.HOME
import java.io.File
import java.util.jar.JarFile

/**
 * Reads Lunar Client's jar files from ~/.lunarclient/offline/multiver and stores the class bytes in a map.
 */
object LunarClassHelper {
    val multiver = File("$HOME/.lunarclient/offline/multiver")

    /**
     * Stores the class bytes of Lunar Client's jar files.
     *
     * If the class bytes are not already stored, it will read the jar file and store the class bytes.
     * The key is the jar file name, and the value is a map of class names to class bytes.
     */
    val classBytes = object : LinkedHashMap<String, Map<String, ByteArray>>() {
        override fun get(key: String): Map<String, ByteArray>? {
            if (!containsKey(key)) {
                readJar(key)
            }

            return super.get(key)
        }
    }

    /**
     * Reads a jar file and stores the class bytes in [classBytes].
     *
     * Only classes in the com.moonsworth.lunar package are stored.
     *
     * @param jarName The name of the jar file to read.
     * @see classBytes
     */
    private fun readJar(jarName: String) {
        val lunarJar = File(multiver, jarName)
        val lunarJarClasses = mutableMapOf<String, ByteArray>()
        val jarFile = JarFile(lunarJar)
        jarFile.entries().asSequence().forEach {
            if (it.name.startsWith("com/moonsworth/lunar/") && it.name.endsWith(".class")) {
                val inputStream = jarFile.getInputStream(it)
                val bytes = inputStream.readBytes()
                lunarJarClasses[it.name.substring(0, it.name.length - 6)] = bytes
            }
        }
        classBytes[lunarJar.name] = lunarJarClasses
    }
}
