val projectName: String by settings
rootProject.name = projectName

pluginManagement {
    repositories {
        mavenLocal()
        gradlePluginPortal()
        maven("https://gitlab.com/api/v4/projects/57327439/packages/maven") // Stellar
        maven("https://gitlab.com/api/v4/projects/57325214/packages/maven") // Weave Gradle
        maven("https://jitpack.io")
        maven("https://repo.weavemc.dev/releases")
    }
}

include("v1_8")
include("lunar")
