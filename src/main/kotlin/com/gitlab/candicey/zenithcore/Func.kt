package com.gitlab.candicey.zenithcore

internal fun info(message: String) = versionCompatibilityAdaptor.`logger$info`(message)
internal fun warn(message: String) = versionCompatibilityAdaptor.`logger$warn`(message)

/**
 * Checks if a string is a valid json.
 */
fun isJsonValid(json: String): Boolean = runCatching { JSON_PARSER.parse(json) }.isSuccess

fun mapClass(name: String): String = versionCompatibilityAdaptor.`mappings$mapper`.map(name) ?: name
fun unmapClass(name: String): String = versionCompatibilityAdaptor.`mappings$unmapper`.map(name) ?: name