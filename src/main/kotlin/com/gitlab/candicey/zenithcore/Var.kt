package com.gitlab.candicey.zenithcore

import com.gitlab.candicey.zenithcore.common.IVersionCompatibilityAdaptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import kotlinx.serialization.json.Json
import java.lang.instrument.Instrumentation
import java.util.*

val ZENITH_CORE_VERSION: String = "@@ZENITH_CORE_VERSION@@"

val ZENITH_CORE_PACKAGE: String = ZenithCoreMain::class.java.run { `package`?.name ?: name.substringBeforeLast('.') }

val ZENITH_CORE_VERSION_ENTRYPOINT_CLASS_PROPERTY: String = "zenithcore.v${ZENITH_CORE_VERSION.replace('.', '_')}.versionEntryPointClass"

val SYSTEM_PROPERTIES: Properties get() = System.getProperties()

val HOME: String by lazy { SYSTEM_PROPERTIES.getProperty("user.home") }

val RANDOM: Random by lazy { Random() }

val JSON: Json by lazy { Json { ignoreUnknownKeys = true } }

val GSON: Gson by lazy { Gson() }
val GSON_PRETTY: Gson by lazy { GsonBuilder().setPrettyPrinting().create() }

val JSON_PARSER: JsonParser by lazy { JsonParser() }

var versionCompatibilityAdaptor: IVersionCompatibilityAdaptor = IVersionCompatibilityAdaptor

/**
 * Regex for checking if a string is a valid Minecraft username. (3-16 characters, letters, numbers, and underscores only)
 */
val nameRegex: Regex by lazy { Regex("[a-zA-Z0-9_]{3,16}") }

var instrumentation: Instrumentation? = null
    internal set

val mainLoader: ClassLoader = ZenithCoreMain::class.java.classLoader
