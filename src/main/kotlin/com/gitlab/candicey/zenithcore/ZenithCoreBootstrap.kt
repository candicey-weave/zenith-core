package com.gitlab.candicey.zenithcore

import com.gitlab.candicey.zenithcore.util.PropertyDelegate
import java.lang.instrument.Instrumentation
import java.util.function.Consumer

class ZenithCoreBootstrap : Consumer<Instrumentation?> {
    var bootstrapped: Boolean by PropertyDelegate(
        property = "zenithcore.v${ZENITH_CORE_VERSION.replace('.', '_')}.bootstrapped",
        defaultValue = { false }
    )

    override fun accept(inst: Instrumentation?) {
        if (bootstrapped) {
            return
        }

        require(inst != null) { "Instrumentation is null" }

        ZenithCoreMain().preInit(inst)

        bootstrapped = true
    }
}