package com.gitlab.candicey.zenithcore

import com.gitlab.candicey.zenithcore.common.IVersionEntryPoint
import com.gitlab.candicey.zenithcore.helper.FileHelper
import net.weavemc.api.ModInitializer
import java.lang.instrument.Instrumentation

class ZenithCoreMain : ModInitializer {
    companion object {
        private var initialised = false
    }

    override fun preInit(inst: Instrumentation) {
        if (initialised) {
            return
        }
        initialised = true

        info("Initialising...")

        instrumentation = inst

        FileHelper.cleanParentTempDirectory()
        Runtime.getRuntime().addShutdownHook(Thread(FileHelper::deleteTempDirectory))

        info("Subscribing events...")
        info("Events subscribed!")

        initialiseVersionEntryPoint()

        info("Initialisation complete!")
    }

    private fun initialiseVersionEntryPoint() {
        val versionEntryPointClasses = System.getProperties()[ZENITH_CORE_VERSION_ENTRYPOINT_CLASS_PROPERTY]
            ?.let { (if (it !is List<*>) listOf(it.toString()) else it) as List<String> }
            ?: run {
                info("No version entry point class specified, skipping initialisation")
                return
            }

        for (versionEntryPointClass in versionEntryPointClasses) {
            info("Initialising version entrypoint: $versionEntryPointClass")

            val versionEntryPoint = mainLoader
                .loadClass(versionEntryPointClass)
                .getDeclaredConstructor()
                .newInstance() as IVersionEntryPoint
            versionEntryPoint.versionCompatibilityAdaptor?.let {
                versionCompatibilityAdaptor = it
                info("Version compatibility adaptor set to: ${it::class.java.name}")
            }
            versionEntryPoint.init()

            info("Version entrypoint initialised: $versionEntryPointClass")
        }
    }
}