package com.gitlab.candicey.zenithcore.common

import com.gitlab.candicey.zenithcore.ZENITH_CORE_VERSION
import org.objectweb.asm.commons.Remapper

interface IVersionCompatibilityAdaptor {
    val `mappings$namespace`: String
    val `mappings$mapper`: Remapper
    val `mappings$unmapper`: Remapper

    fun `logger$info`(message: String)
    fun `logger$warn`(message: String)

    companion object Default : IVersionCompatibilityAdaptor {
        override val `mappings$namespace`: String = ""
        override val `mappings$mapper`: Remapper = object : Remapper() {}
        override val `mappings$unmapper`: Remapper = object : Remapper() {}

        override fun `logger$info`(message: String) = println("[Zenith-Core (${ZENITH_CORE_VERSION})] [INFO] $message")
        override fun `logger$warn`(message: String) = println("[Zenith-Core (${ZENITH_CORE_VERSION})] [WARN] $message")
    }
}