package com.gitlab.candicey.zenithcore.common

interface IVersionEntryPoint {
    val versionCompatibilityAdaptor: IVersionCompatibilityAdaptor?
        get() = null

    fun preInit() {
    }

    fun init() {
    }
}