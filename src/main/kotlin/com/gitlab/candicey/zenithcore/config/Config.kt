package com.gitlab.candicey.zenithcore.config

import com.gitlab.candicey.zenithcore.GSON
import com.gitlab.candicey.zenithcore.GSON_PRETTY
import com.gitlab.candicey.zenithcore.util.FieldCache
import com.gitlab.candicey.zenithcore.util.reflectField
import java.io.File
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KProperty

/**
 * A config file.
 *
 * @property configFile The config's [File].
 * @property config The config's data class instance.
 * @see [ConfigManager]
 */
open class Config<T : Any> (
    val configFile: File,
    var config: T,
) {
    var autoSave = true

    /**
     * Reads the config file and returns the config's data class instance if it exists, otherwise returns the [default config][config].
     */
    fun readConfig(): T? =
        runCatching { GSON.fromJson(configFile.bufferedReader(), config::class.java) }.getOrDefault(config)

    /**
     * Loads the config.
     *
     * If the config file does not exist, it will be created and the [default config][config] will be written to it.
     */
    fun loadConfig() {
        if (!configFile.exists()) {
            configFile.parentFile.mkdirs()
            configFile.createNewFile().also { writeConfig() }
        }

        config = readConfig() ?: config
    }

    /**
     * Writes the config to the config file.
     */
    fun writeConfig(pretty: Boolean = true) =
        configFile.bufferedWriter().use { it.write((if (pretty) GSON_PRETTY else GSON).toJson(config)) }

    fun <V : Any> delegateValue(property: (T.() -> KMutableProperty0<V>)? = null): ConfigOptionDelegate<T, V> =
        ConfigOptionDelegate(this, property, true)

    operator fun <V> getValue(instance: Any, kProperty: KProperty<*>): V = getFieldCache(kProperty.name).get(config) as V

    operator fun <V> setValue(instance: Any, kProperty: KProperty<*>, value: V) {
        getFieldCache(kProperty.name)[config] = value

        if (autoSave) {
            writeConfig()
        }
    }

    operator fun <V> getValue(nothing: Nothing?, kProperty: KProperty<*>): V = getFieldCache(kProperty.name).get(config) as V

    operator fun <V> setValue(nothing: Nothing?, kProperty: KProperty<*>, value: V) {
        getFieldCache(kProperty.name).set(config, value)

        if (autoSave) {
            writeConfig()
        }
    }

    private fun getFieldCache(name: String): FieldCache = reflectField(config::class.java, name)
}
