package com.gitlab.candicey.zenithcore.config

import com.gitlab.candicey.zenithcore.util.DelegateHolder

/**
 * A class that manages [configs][Config].
 *
 * @param configs A map of all configs. The key is the name of the config (e.g. "keybinds"). The value is the [Config] object.
 *
 * @see [Config]
 */
class ConfigManager(val configs: Map<String, Config<*>>) {
    /**
     * Loads all configs.
     */
    fun init() = configs.forEach { (_, config) -> config.loadConfig() }

    /**
     * Saves all configs.
     */
    fun save() = configs.forEach { (_, config) -> config.writeConfig() }

    /**
     * Gets a config from the [configs] map by the type [T].
     *
     * @param T The type of the config.
     * @return The config with the type [T] or null if not found.
     */
    inline fun <reified T : Any> getConfig(): Config<T>? =
        configs.filter { it.value.config is T }.values.firstOrNull() as Config<T>?

    /**
     * Gets a config object from the [configs] map by the type [T].
     *
     * @param T The type of the config.
     * @return The config object with the type [T] or null if not found.
     */
    inline fun <reified T : Any> getConfigObject(): T? = getConfig<T>()?.config

    /**
     * Gets a config from the [configs] map by [name].
     *
     * @param name The name of the config.
     * @return The config with the name [name] or null if not found.
     */
    fun getConfig(name: String): Config<*>? = configs[name]

    inline fun <reified T : Any> delegateConfig(): DelegateHolder<Config<T>> = DelegateHolder(getConfig<T>()!!)

    companion object {
        inline fun <reified T : Any> delegateConfig(configManager: ConfigManager): DelegateHolder<Config<T>> = DelegateHolder(configManager.getConfig<T>()!!)
    }
}