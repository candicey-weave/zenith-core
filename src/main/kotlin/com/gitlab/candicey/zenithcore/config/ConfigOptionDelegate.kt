package com.gitlab.candicey.zenithcore.config

import com.gitlab.candicey.zenithcore.util.reflectField
import kotlin.reflect.KMutableProperty0
import kotlin.reflect.KProperty

class ConfigOptionDelegate<T : Any, V : Any>(val config: Config<T>, val property: (T.() -> KMutableProperty0<V>)? = null, val writeConfigPretty: Boolean = true) {
    private var configOption = property?.let { config.config.it() }

    private fun get(kProperty: KProperty<*>): V =
        if (configOption != null) {
            configOption!!.get()
        } else {
            with(config.config) {
                reflectField(kProperty.name)[this] as V
            }
        }

    private fun set(kProperty: KProperty<*>, value: V) {
        if (configOption != null) {
            configOption!!.set(value)
        } else {
            with(config.config) {
                reflectField(kProperty.name)[this] = value
            }
        }

        config.writeConfig(pretty = writeConfigPretty)
    }

    operator fun getValue(thisRef: Nothing?, property: KProperty<*>): V = get(property)

    operator fun setValue(thisRef: Nothing?, property: KProperty<*>, value: V) = set(property, value)

    operator fun getValue(thisRef: Any?, property: KProperty<*>): V = get(property)

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: V) = set(property, value)
}