package com.gitlab.candicey.zenithcore.data

import org.objectweb.asm.tree.LdcInsnNode

sealed class LdcConstant {
    var ldcInsnNode: LdcInsnNode? = null

    data class Integer(val value: Int) : LdcConstant()

    data class Float(val value: kotlin.Float) : LdcConstant()

    data class Long(val value: kotlin.Long) : LdcConstant()

    data class Double(val value: kotlin.Double) : LdcConstant()

    data class String(val value: kotlin.String) : LdcConstant()

    data class Type(val value: org.objectweb.asm.Type) : LdcConstant()

    data class Handle(val value: org.objectweb.asm.Handle) : LdcConstant()

    data class ConstantDynamic(val value: org.objectweb.asm.ConstantDynamic) : LdcConstant()

    companion object {
        fun from(ldcInsnNode: LdcInsnNode): LdcConstant =
            when (val cst = ldcInsnNode.cst) {
                is Int -> Integer(cst)
                is kotlin.Float -> Float(cst)
                is kotlin.Long -> Long(cst)
                is kotlin.Double -> Double(cst)
                is kotlin.String -> String(cst)
                is org.objectweb.asm.Type -> Type(cst)
                is org.objectweb.asm.Handle -> Handle(cst)
                is org.objectweb.asm.ConstantDynamic -> ConstantDynamic(cst)
                else -> error("Unknown constant type: ${cst::class.java.name}")
            }.also { it.ldcInsnNode = ldcInsnNode }
    }
}