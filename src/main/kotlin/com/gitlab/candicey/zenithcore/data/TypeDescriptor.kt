package com.gitlab.candicey.zenithcore.data

@JvmInline
value class TypeDescriptor(val value: String) {
    /**
     * Returns the size of the type on the stack.
     * If the type is a long or double, 2 is returned, otherwise 1.
     */
    val stackSize: Int
        get() = when (this.value) {
            "J", "D" -> 2
            else -> 1
        }
}