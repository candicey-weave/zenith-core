package com.gitlab.candicey.zenithcore.enum

enum class InjectPosition {
    HEAD,
    RETURN,
}