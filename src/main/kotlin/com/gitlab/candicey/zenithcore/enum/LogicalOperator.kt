package com.gitlab.candicey.zenithcore.enum

enum class LogicalOperator {
    AND,
    OR,
}