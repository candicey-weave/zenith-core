package com.gitlab.candicey.zenithcore.enum

enum class SidePosition {
    TOP,
    BOTTOM,
    LEFT,
    RIGHT,
}