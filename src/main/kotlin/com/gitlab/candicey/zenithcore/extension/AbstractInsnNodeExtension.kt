package com.gitlab.candicey.zenithcore.extension

import org.objectweb.asm.Opcodes.*
import org.objectweb.asm.tree.*

/**
 * Returns the [AbstractInsnNode] in a readable format.
 */
val AbstractInsnNode.readableString: String
    get() = when (this) {
        is FieldInsnNode -> {
            when (opcode) {
                GETFIELD -> "GETFIELD $owner $name $desc"
                GETSTATIC -> "GETSTATIC $owner $name $desc"
                PUTFIELD -> "PUTFIELD $owner $name $desc"
                PUTSTATIC -> "PUTSTATIC $owner $name $desc"
                else -> error("Invalid opcode: $opcode")
            }
        }

        is IincInsnNode -> {
            "IINC $`var` $incr"
        }

        is InsnNode -> {
            when (opcode) {
                NOP -> "NOP"
                ACONST_NULL -> "ACONST_NULL"
                ICONST_M1 -> "ICONST_M1"
                ICONST_0 -> "ICONST_0"
                ICONST_1 -> "ICONST_1"
                ICONST_2 -> "ICONST_2"
                ICONST_3 -> "ICONST_3"
                ICONST_4 -> "ICONST_4"
                ICONST_5 -> "ICONST_5"
                LCONST_0 -> "LCONST_0"
                LCONST_1 -> "LCONST_1"
                FCONST_0 -> "FCONST_0"
                FCONST_1 -> "FCONST_1"
                FCONST_2 -> "FCONST_2"
                DCONST_0 -> "DCONST_0"
                DCONST_1 -> "DCONST_1"
                IALOAD -> "IALOAD"
                LALOAD -> "LALOAD"
                FALOAD -> "FALOAD"
                DALOAD -> "DALOAD"
                AALOAD -> "AALOAD"
                BALOAD -> "BALOAD"
                CALOAD -> "CALOAD"
                SALOAD -> "SALOAD"
                IASTORE -> "IASTORE"
                LASTORE -> "LASTORE"
                FASTORE -> "FASTORE"
                DASTORE -> "DASTORE"
                AASTORE -> "AASTORE"
                BASTORE -> "BASTORE"
                CASTORE -> "CASTORE"
                SASTORE -> "SASTORE"
                POP -> "POP"
                POP2 -> "POP2"
                DUP -> "DUP"
                DUP_X1 -> "DUP_X1"
                DUP_X2 -> "DUP_X2"
                DUP2 -> "DUP2"
                DUP2_X1 -> "DUP2_X1"
                DUP2_X2 -> "DUP2_X2"
                SWAP -> "SWAP"
                IADD -> "IADD"
                LADD -> "LADD"
                FADD -> "FADD"
                DADD -> "DADD"
                ISUB -> "ISUB"
                LSUB -> "LSUB"
                FSUB -> "FSUB"
                DSUB -> "DSUB"
                IMUL -> "IMUL"
                LMUL -> "LMUL"
                FMUL -> "FMUL"
                DMUL -> "DMUL"
                IDIV -> "IDIV"
                LDIV -> "LDIV"
                FDIV -> "FDIV"
                DDIV -> "DDIV"
                IREM -> "IREM"
                LREM -> "LREM"
                FREM -> "FREM"
                DREM -> "DREM"
                INEG -> "INEG"
                LNEG -> "LNEG"
                FNEG -> "FNEG"
                DNEG -> "DNEG"
                ISHL -> "ISHL"
                LSHL -> "LSHL"
                ISHR -> "ISHR"
                LSHR -> "LSHR"
                IUSHR -> "IUSHR"
                LUSHR -> "LUSHR"
                IAND -> "IAND"
                LAND -> "LAND"
                IOR -> "IOR"
                LOR -> "LOR"
                IXOR -> "IXOR"
                LXOR -> "LXOR"
                I2L -> "I2L"
                I2F -> "I2F"
                I2D -> "I2D"
                L2I -> "L2I"
                L2F -> "L2F"
                L2D -> "L2D"
                F2I -> "F2I"
                F2L -> "F2L"
                F2D -> "F2D"
                D2I -> "D2I"
                D2L -> "D2L"
                D2F -> "D2F"
                I2B -> "I2B"
                I2C -> "I2C"
                I2S -> "I2S"
                LCMP -> "LCMP"
                FCMPL -> "FCMPL"
                FCMPG -> "FCMPG"
                DCMPL -> "DCMPL"
                DCMPG -> "DCMPG"
                IRETURN -> "IRETURN"
                LRETURN -> "LRETURN"
                FRETURN -> "FRETURN"
                DRETURN -> "DRETURN"
                ARETURN -> "ARETURN"
                RETURN -> "RETURN"
                ARRAYLENGTH -> "ARRAYLENGTH"
                ATHROW -> "ATHROW"
                MONITORENTER -> "MONITORENTER"
                MONITOREXIT -> "MONITOREXIT"
                else -> error("Invalid opcode: $opcode")
            }
        }

        is IntInsnNode -> {
            when (opcode) {
                BIPUSH -> "BIPUSH $operand"
                SIPUSH -> "SIPUSH $operand"
                NEWARRAY -> "NEWARRAY $operand"
                else -> error("Invalid opcode: $opcode")
            }
        }

        is InvokeDynamicInsnNode -> {
            "INVOKEDYNAMIC $name $desc $bsm ${bsmArgs.toList()}"
        }

        is JumpInsnNode -> {
            when (opcode) {
                IFEQ -> "IFEQ $label"
                IFNE -> "IFNE $label"
                IFLT -> "IFLT $label"
                IFGE -> "IFGE $label"
                IFGT -> "IFGT $label"
                IFLE -> "IFLE $label"
                IF_ICMPEQ -> "IF_ICMPEQ $label"
                IF_ICMPNE -> "IF_ICMPNE $label"
                IF_ICMPLT -> "IF_ICMPLT $label"
                IF_ICMPGE -> "IF_ICMPGE $label"
                IF_ICMPGT -> "IF_ICMPGT $label"
                IF_ICMPLE -> "IF_ICMPLE $label"
                IF_ACMPEQ -> "IF_ACMPEQ $label"
                IF_ACMPNE -> "IF_ACMPNE $label"
                GOTO -> "GOTO $label"
                JSR -> "JSR $label"
                IFNULL -> "IFNULL $label"
                IFNONNULL -> "IFNONNULL $label"
                else -> error("Invalid opcode: $opcode")
            }
        }

        is LabelNode -> {
            "LABEL $label"
        }

        is LdcInsnNode -> {
            "LDC $cst"
        }

        is LineNumberNode -> {
            "LINENUMBER $line $start"
        }

        is LookupSwitchInsnNode -> {
            "LOOKUPSWITCH $dflt $keys $labels"
        }

        is MethodInsnNode -> {
            when (opcode) {
                INVOKEVIRTUAL -> "INVOKEVIRTUAL $owner $name $desc $itf"
                INVOKESPECIAL -> "INVOKESPECIAL $owner $name $desc $itf"
                INVOKESTATIC -> "INVOKESTATIC $owner $name $desc $itf"
                INVOKEINTERFACE -> "INVOKEINTERFACE $owner $name $desc $itf"
                else -> error("Invalid opcode: $opcode")
            }
        }

        is MultiANewArrayInsnNode -> {
            "MULTIANEWARRAY $desc $dims"
        }

        is TableSwitchInsnNode -> {
            "TABLESWITCH $min $max $dflt $labels"
        }

        is TypeInsnNode -> {
            when (opcode) {
                NEW -> "NEW $desc"
                ANEWARRAY -> "ANEWARRAY $desc"
                CHECKCAST -> "CHECKCAST $desc"
                INSTANCEOF -> "INSTANCEOF $desc"
                else -> error("Invalid opcode: $opcode")
            }
        }

        is VarInsnNode -> {
            when (opcode) {
                ILOAD -> "ILOAD $`var`"
                LLOAD -> "LLOAD $`var`"
                FLOAD -> "FLOAD $`var`"
                DLOAD -> "DLOAD $`var`"
                ALOAD -> "ALOAD $`var`"
                ISTORE -> "ISTORE $`var`"
                LSTORE -> "LSTORE $`var`"
                FSTORE -> "FSTORE $`var`"
                DSTORE -> "DSTORE $`var`"
                ASTORE -> "ASTORE $`var`"
                RET -> "RET $`var`"
                else -> error("Invalid opcode: $opcode")
            }
        }

        else -> error("Invalid opcode: $opcode")
    }