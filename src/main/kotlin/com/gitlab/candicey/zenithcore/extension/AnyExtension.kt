package com.gitlab.candicey.zenithcore.extension

import net.weavemc.api.event.EventBus
import org.objectweb.asm.Type

val Any.internalName: String
    get() = Type.getInternalName(this::class.java)

fun Any.subscribeEventListener() = EventBus.subscribe(this)