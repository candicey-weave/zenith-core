package com.gitlab.candicey.zenithcore.extension

import org.objectweb.asm.ClassReader
import org.objectweb.asm.tree.ClassNode

fun ByteArray.parse(): Pair<ClassReader, ClassNode> {
    val classReader = ClassReader(this)
    val classNode = ClassNode()
    classReader.accept(classNode, 0)
    return classReader to classNode
}

fun ByteArray.toClassReader(): ClassReader = ClassReader(this)

fun ByteArray.toClassNode(): ClassNode = ClassNode().also { ClassReader(this).accept(it, 0) }