package com.gitlab.candicey.zenithcore.extension

fun CharSequence.contains(vararg elements: CharSequence, ignoreCase: Boolean = false): Boolean {
    return elements.any { contains(it, ignoreCase) }
}