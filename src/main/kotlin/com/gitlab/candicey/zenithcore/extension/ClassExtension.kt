package com.gitlab.candicey.zenithcore.extension

import java.lang.reflect.Field
import java.lang.reflect.Method

/**
 * Returns the descriptor of this class.
 */
val Class<*>.descriptor: String
    get() = when {
        isPrimitive -> when (this) {
            Boolean::class.javaPrimitiveType -> "Z"
            Char::class.javaPrimitiveType -> "C"
            Byte::class.javaPrimitiveType -> "B"
            Short::class.javaPrimitiveType -> "S"
            Int::class.javaPrimitiveType -> "I"
            Long::class.javaPrimitiveType -> "J"
            Float::class.javaPrimitiveType -> "F"
            Double::class.javaPrimitiveType -> "D"
            Void::class.javaPrimitiveType -> "V"
            else -> throw IllegalArgumentException("Unknown primitive type: $this")
        }

        isArray -> "[${componentType.descriptor}"

        else -> "L${name.replace('.', '/')};"
    }

val Class<*>.slashName: String
    get() = name.replace('.', '/')

val Class<*>.allMethods: List<Method>
    get() = declaredMethods.toMutableList().also {
        if (superclass != null) {
            it += superclass.allMethods
        }
    }

fun Class<*>.getAllMethod(name: String, vararg parameterTypes: Class<*>, returnType: Class<*>? = null): Method? =
    allMethods.find { it.name == name && it.parameterTypes.contentEquals(parameterTypes) && (returnType == null || it.returnType == returnType) }

val Class<*>.allFields: List<Field>
    get() = declaredFields.toMutableList().also {
        if (superclass != null) {
            it += superclass.allFields
        }
    }

fun Class<*>.getAllField(name: String): Field? =
    allFields.find { it.name == name }

val Class<*>.allInterfaces: List<Class<*>>
    get() = interfaces.toMutableList().also {
        if (superclass != null) {
            it += superclass.allInterfaces
        }
    }

fun Class<*>.getAllInterface(name: String): Class<*>? =
    allInterfaces.find { it.name == name }

val Class<*>.allSuperclasses: List<Class<*>>
    get() = mutableListOf<Class<*>>().also {
        var current: Class<*>? = this

        while (current != null) {
            it += current
            current = current.superclass
        }
    }

fun Class<*>.getAllSuperclass(name: String): Class<*>? =
    allSuperclasses.find { it.name == name }