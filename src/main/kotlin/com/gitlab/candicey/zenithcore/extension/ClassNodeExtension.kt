package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.enum.LogicalOperator
import com.gitlab.candicey.zenithcore.warn
import net.weavemc.internals.named
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode
import org.objectweb.asm.tree.MethodNode

fun ClassNode.addFieldSafe(vararg fieldNodes: FieldNode) {
    for (fieldNode in fieldNodes) {
        if (fields.map { field -> field.name }.contains(fieldNode.name)) {
            warn("Field ${fieldNode.name} already exists in class ${name}.")
            continue
        }

        fields.add(fieldNode)
    }
}

/**
 * Checks if the class has any ldc instructions with the given string.
 *
 * @param string The string to check for.
 */
fun ClassNode.hasString(string: String): Boolean {
    return methods.any { it.hasString(string) }
}

/**
 * Checks if the class has [any][LogicalOperator.OR]/[all][LogicalOperator.AND] ldc instructions with the given strings.
 *
 * @param strings The strings to check for.
 * @param logicalOperator The logic operator to use.
 */
fun ClassNode.hasStrings(vararg strings: String, logicalOperator: LogicalOperator = LogicalOperator.AND): Boolean {
    return methods.map { it.stringLdc }.flatten().let { ldc ->
        when (logicalOperator) {
            LogicalOperator.OR -> strings.any { ldc.any { ldcConstant -> ldcConstant.value == it } }
            LogicalOperator.AND -> strings.all { ldc.any { ldcConstant -> ldcConstant.value == it } }
        }
    }
}

fun ClassNode.transform(methodName: String, transformer: MethodNode.() -> Unit) = transformer(methods.named(methodName))

fun ClassNode.removeAnnotations(vararg annotation: String) {
    visibleAnnotations?.removeIf { it.desc in annotation }
    invisibleAnnotations?.removeIf { it.desc in annotation }

    for (method in methods) {
        method.visibleAnnotations?.removeIf { it.desc in annotation }
        method.invisibleAnnotations?.removeIf { it.desc in annotation }
    }

    for (field in fields) {
        field.visibleAnnotations?.removeIf { it.desc in annotation }
        field.invisibleAnnotations?.removeIf { it.desc in annotation }
    }
}