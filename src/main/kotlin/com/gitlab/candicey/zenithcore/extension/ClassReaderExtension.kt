package com.gitlab.candicey.zenithcore.extension

import org.objectweb.asm.ClassReader
import org.objectweb.asm.tree.ClassNode

fun ClassReader.toClassNode() = ClassNode().also { accept(it, 0) }