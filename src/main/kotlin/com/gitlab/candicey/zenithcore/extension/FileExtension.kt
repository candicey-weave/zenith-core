package com.gitlab.candicey.zenithcore.extension

import java.io.File
import java.security.MessageDigest

val File.sha256: String
    get() = MessageDigest
        .getInstance("SHA-256")
        .digest(readBytes())
        .joinToString("") { "%02x".format(it) }