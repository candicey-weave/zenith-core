package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.versionCompatibilityAdaptor
import net.weavemc.api.Hook
import net.weavemc.loader.InjectionHandler
import net.weavemc.loader.ModHook

fun InjectionHandler.registerHook(hook: Hook, namespace: String = versionCompatibilityAdaptor.`mappings$namespace`) =
    registerModifier(ModHook(namespace, hook))