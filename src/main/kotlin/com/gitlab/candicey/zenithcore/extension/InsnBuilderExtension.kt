package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.data.TypeDescriptor
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import net.weavemc.internals.InsnBuilder
import org.objectweb.asm.Opcodes
import org.objectweb.asm.Type
import org.objectweb.asm.tree.InsnNode
import org.objectweb.asm.tree.MethodNode
import kotlin.reflect.KFunction
import kotlin.reflect.KProperty

fun InsnBuilder.opcode(opcode: Int) = +InsnNode(opcode)

/**
 * Loads a local variable from the stack based on the [type descriptor][typeDescriptor].
 */
fun InsnBuilder.load(index: Int, typeDescriptor: TypeDescriptor) =
    when (typeDescriptor.value) {
        "Z", "B", "C", "S", "I" -> iload(index)
        "J" -> lload(index)
        "F" -> fload(index)
        "D" -> dload(index)
        else -> aload(index)
    }

fun InsnBuilder.returnType(type: Type) = +InsnNode(type.getOpcode(Opcodes.IRETURN))
fun InsnBuilder.returnType(typeDescriptor: String) = returnType(Type.getReturnType(typeDescriptor))
fun InsnBuilder.returnType(methodNode: MethodNode) = returnType(methodNode.desc)

/**
 * Pushes a value onto the stack based on the [value]'s type.
 *
 * @param value The value to push onto the stack.
 */
fun InsnBuilder.value(value: Any?) =
    when (value) {
        null -> aconst_null

        is Boolean -> if (value) iconst_1 else iconst_0

        is Byte -> bipush(value.toInt())

        is Short -> sipush(value.toInt())

        is Int -> when (value) {
            -1 -> iconst_m1
            0 -> iconst_0
            1 -> iconst_1
            2 -> iconst_2
            3 -> iconst_3
            4 -> iconst_4
            5 -> iconst_5
            else -> when {
                value >= -128 && value <= 127 -> bipush(value)
                value >= -32768 && value <= 32767 -> sipush(value)
                else -> ldc(value)
            }
        }

        is Long -> when (value) {
            0L -> lconst_0
            1L -> lconst_1
            else -> ldc(value)
        }

        is Float -> when (value) {
            0F -> fconst_0
            1F -> fconst_1
            2F -> fconst_2
            else -> ldc(value)
        }

        is Double -> when (value) {
            0.0 -> dconst_0
            1.0 -> dconst_1
            else -> ldc(value)
        }

        is String -> ldc(value)

        else -> throw IllegalArgumentException("Unsupported value type: ${value::class.java}")
    }

inline fun <reified T : Any> InsnBuilder.invokevirtual(method: KFunction<*>) = invokevirtual(internalNameOf<T>(), method.name, method.descriptor)
inline fun <reified T : Any> InsnBuilder.invokestatic(method: KFunction<*>) = invokestatic(internalNameOf<T>(), method.name, method.descriptor)
inline fun <reified T : Any> InsnBuilder.invokespecial(method: KFunction<*>) = invokespecial(internalNameOf<T>(), method.name, method.descriptor)
inline fun <reified T : Any> InsnBuilder.invokeinterface(method: KFunction<*>) = invokeinterface(internalNameOf<T>(), method.name, method.descriptor)

inline fun <reified T : Any> InsnBuilder.getstatic(field: KProperty<*>) = getstatic(internalNameOf<T>(), field.name, field.descriptor)
inline fun <reified T : Any> InsnBuilder.putstatic(field: KProperty<*>) = putstatic(internalNameOf<T>(), field.name, field.descriptor)
inline fun <reified T : Any> InsnBuilder.getfield(field: KProperty<*>) = getfield(internalNameOf<T>(), field.name, field.descriptor)
inline fun <reified T : Any> InsnBuilder.putfield(field: KProperty<*>) = putfield(internalNameOf<T>(), field.name, field.descriptor)