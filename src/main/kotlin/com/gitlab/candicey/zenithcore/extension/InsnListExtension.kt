package com.gitlab.candicey.zenithcore.extension

import org.objectweb.asm.Opcodes.RETURN
import org.objectweb.asm.tree.AbstractInsnNode
import org.objectweb.asm.tree.InsnList

fun InsnList.subList(startIndex: Int = 0, endIndex: Int = size()) = buildList<AbstractInsnNode> {
    for (i in startIndex ..< endIndex) {
        add(get(i))
    }
}

/**
 * Inserts [insnList] before the last return instruction.
 */
fun InsnList.insertBeforeReturn(insnList: InsnList) {
    val returnInsn = findLast { it.opcode == RETURN } ?: return
    insertBefore(returnInsn, insnList)
}

val InsnList.readableString: String
    get() = joinToString("\n") { it.readableString }