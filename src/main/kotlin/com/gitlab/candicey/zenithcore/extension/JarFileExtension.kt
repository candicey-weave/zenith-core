package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.JSON
import kotlinx.serialization.json.Json
import net.weavemc.internals.ModConfig
import java.util.jar.JarFile

fun JarFile.fetchModConfig(json: Json): ModConfig {
    val configEntry = getEntry("weave.mod.json") ?: error("${this.name} does not contain a weave.mod.json!")
    return json.decodeFromString<ModConfig>(getInputStream(configEntry).readBytes().decodeToString())
}

fun JarFile.config() = runCatching { fetchModConfig(JSON) }.getOrNull()