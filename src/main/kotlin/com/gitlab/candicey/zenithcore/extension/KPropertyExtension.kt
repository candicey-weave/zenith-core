package com.gitlab.candicey.zenithcore.extension

import kotlin.reflect.KProperty

val KProperty<*>.descriptor: String
    get() = returnType.classDescriptor