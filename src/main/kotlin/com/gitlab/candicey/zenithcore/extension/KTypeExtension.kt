package com.gitlab.candicey.zenithcore.extension

import kotlin.reflect.KClass
import kotlin.reflect.KType

val KType.classDescriptor: String // TODO
    get() = when (val type = (classifier as? KClass<*>)?.java!!) {
        Array::class.javaObjectType -> "[${arguments[0].type!!.classDescriptor}"
        Boolean::class.javaPrimitiveType -> "Z"
        Char::class.javaPrimitiveType -> "C"
        Byte::class.javaPrimitiveType -> "B"
        Short::class.javaPrimitiveType -> "S"
        Int::class.javaPrimitiveType -> "I"
        Float::class.javaPrimitiveType -> "F"
        Long::class.javaPrimitiveType -> "J"
        Double::class.javaPrimitiveType -> "D"
        Void::class.javaPrimitiveType -> "V"
        else -> if (type.isArray) type.name.replace('.', '/') else "L${type.name.replace('.', '/')};"
    }


/**
 * If the return type is Unit, return `V` instead of `Lkotlin/Unit;`.
 */
val KType.classDescriptorReturnType: String
    get() = if (classDescriptor == "Lkotlin/Unit;") "V" else classDescriptor