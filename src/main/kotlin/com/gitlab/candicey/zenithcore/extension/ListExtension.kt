package com.gitlab.candicey.zenithcore.extension

fun List<String>.trim(): List<String> = map { it.trim() }

fun <T> List<T>.previous(element: T): T? =
    when (val index = indexOf(element)) {
        -1 -> throw IndexOutOfBoundsException()
        0 -> null
        else -> get(index - 1)
    }

fun <T> List<T>.next(element: T): T? =
    when (val index = indexOf(element)) {
        -1 -> throw IndexOutOfBoundsException()
        size - 1 -> null
        else -> get(index + 1)
    }

inline fun <reified R> List<*>.findIsInstance(): R? = find { it is R } as? R