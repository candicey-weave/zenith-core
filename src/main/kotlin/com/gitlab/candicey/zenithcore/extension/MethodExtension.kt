package com.gitlab.candicey.zenithcore.extension

import org.objectweb.asm.tree.MethodNode
import java.lang.reflect.Method

fun MethodNode.clear() {
    instructions.clear()
    localVariables.clear()
    tryCatchBlocks.clear()
}

/**
 * Gets the descriptor of a method.
 *
 * @return The descriptor of the method.
 * @see [Class.toDescriptor]
 */
val Method.descriptor: String
    get() {
        val parameterTypes = parameterTypes
        val parameterTypeDescriptors = parameterTypes.joinToString { it.descriptor }
        val returnTypeDescriptor = returnType.descriptor
        return "($parameterTypeDescriptors)$returnTypeDescriptor"
    }