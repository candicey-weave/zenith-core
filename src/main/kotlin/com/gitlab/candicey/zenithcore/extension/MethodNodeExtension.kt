package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.data.LdcConstant
import com.gitlab.candicey.zenithcore.enum.LogicalOperator
import com.gitlab.candicey.zenithcore.warn
import org.objectweb.asm.Opcodes.*
import org.objectweb.asm.Type
import org.objectweb.asm.tree.LdcInsnNode
import org.objectweb.asm.tree.MethodNode

val MethodNode.isStatic: Boolean
    get() = access and ACC_STATIC != 0

val MethodNode.isFinal: Boolean
    get() = access and ACC_FINAL != 0

val MethodNode.isAbstract: Boolean
    get() = access and ACC_ABSTRACT != 0

fun MutableList<MethodNode>.addMethodSafe(vararg methodNode: MethodNode) {
    for (method in methodNode) {
        if (any { it.name == method.name && it.desc == method.desc }) {
            warn("Method ${method.name}${method.desc} already exists in class.")
            continue
        }

        add(method)
    }
}

/**
 * Gets the parameters' descriptors of this method as a list of strings.
 *
 * @return The list of parameters' descriptors.
 */
val MethodNode.parametersString: List<String>?
    get() = desc?.let { string -> Type.getArgumentTypes(string).map { it.descriptor } }

/**
 * Checks if this method has the specified [string].
 *
 * @param string The string to check for.
 */
fun MethodNode.hasString(string: String): Boolean
    = stringLdc.any { it.value == string }

/**
 * Checks if this method has [any][LogicalOperator.OR]/[all][LogicalOperator.AND] ldc instructions with the given strings.
 *
 * @param strings The strings to check for.
 * @param logicalOperator The logic operator to use.
 */
fun MethodNode.hasStrings(vararg strings: String, logicalOperator: LogicalOperator = LogicalOperator.AND): Boolean {
    return when (logicalOperator) {
        LogicalOperator.OR -> strings.any { hasString(it) }
        LogicalOperator.AND -> strings.all { hasString(it) }
    }
}

/**
 * All ldc instructions in this method.
 *
 * @return The list of ldc instructions.
 * @see LdcConstant
 */
val MethodNode.ldc: List<LdcConstant>
    get() = instructions.filterIsInstance<LdcInsnNode>().map { LdcConstant.from(it) }

/**
 * All string ldc instructions in this method.
 *
 * @return The list of string ldc instructions.
 * @see LdcConstant.String
 */
val MethodNode.stringLdc: List<LdcConstant.String>
    get() = ldc.filterIsInstance<LdcConstant.String>()