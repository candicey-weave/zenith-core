package com.gitlab.candicey.zenithcore.extension

import com.gitlab.candicey.zenithcore.nameRegex
import java.net.HttpURLConnection
import java.net.URL

/**
 * Deletes the specified string in this string.
 */
fun String.delete(string: String): String = replace(string, "")

/**
 * Deletes the text that matches the specified regular expression in this string.
 */
fun String.delete(regex: Regex): String = replace(regex, "")

/**
 * Deletes the Minecraft formatting code in this string.
 */
fun String.clearedFormattingText(): String = delete(Regex("§[0-9a-fk-or]"))

/**
 * Converts this string to a [HttpURLConnection] object.
 */
fun String.toHttpUrlConnection(): HttpURLConnection = URL(this).openConnection() as HttpURLConnection

/**
 * Checks if this string is a valid Minecraft username. (3-16 characters, only letters, numbers, and underscores)
 *
 * @see [com.gitlab.candicey.zenithcore.nameRegex]
 */
fun String.isValidMinecraftUsername(): Boolean = nameRegex.matches(this)

/**
 * Checks if this string is a valid HTTP(S) URL.
 */
fun String.isHttpUrl(): Boolean = Regex("^(http|https)://.*$").matches(this)

@Throws(NumberFormatException::class)
fun String.toIntIfBlankNullOrThrow(): Int? = if (isBlank()) null else toIntOrNull() ?: throw NumberFormatException("For input string: \"$this\"")

fun String.toIntDefaultIfBlankOrNull(default: Int? = null): Int? = if (isBlank()) default else toIntOrNull()

@Throws(NumberFormatException::class)
fun String.toFloatIfBlankNullOrThrow(): Float? = if (isBlank()) null else toFloatOrNull() ?: throw NumberFormatException("For input string: \"$this\"")

fun String.toFloatDefaultIfBlankOrNull(default: Float? = null): Float? = if (isBlank()) default else toFloatOrNull()