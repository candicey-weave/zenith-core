package com.gitlab.candicey.zenithcore.helper

import java.awt.Image
import java.awt.Toolkit
import java.awt.datatransfer.DataFlavor
import java.awt.datatransfer.StringSelection
import java.awt.datatransfer.Transferable
import java.awt.datatransfer.UnsupportedFlavorException

/**
 * A helper class for setting the clipboard.
 */
object ClipboardHelper {
    val systemClipboard = Toolkit.getDefaultToolkit().systemClipboard

    /**
     * Sets the system clipboard to the specified string.
     */
    fun setClipboardString(string: String) = systemClipboard.setContents(StringSelection(string), null)

    /**
     * Sets the system clipboard to the specified image.
     */
    fun setClipboardImage(image: Image) = systemClipboard.setContents(ImageTransferable(image), null)

    /**
     * A transferable for images.
     *
     * @property image The image to transfer.
     */
    class ImageTransferable(private val image: Image) : Transferable {
        /**
         * Only supported data flavour is image.
         */
        override fun getTransferDataFlavors() = arrayOf(DataFlavor.imageFlavor)

        /**
         * Checks if the specified data flavour is supported (only image is supported).
         */
        override fun isDataFlavorSupported(flavour: DataFlavor?) = transferDataFlavors.contains(flavour)

        override fun getTransferData(flavour: DataFlavor?): Any {
            if (flavour == null || !isDataFlavorSupported(flavour)) {
                throw UnsupportedFlavorException(flavour)
            }

            return image
        }
    }
}