package com.gitlab.candicey.zenithcore.helper

import com.gitlab.candicey.zenithcore.HOME
import java.io.File

/**
 * A helper class for file operations. Mainly used for managing temporary files.
 */
object FileHelper {
    /**
     * A string containing all the characters that can be used to generate a random string. (0-9, a-z, A-Z)
     */
    const val RANDOM_STRING = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

    const val TEMPORARY_DIRECTORY_AGE = 1000 * 60 * 60 * 24 * 7 // 7 days

    val loadedTimestamp = System.nanoTime()

    /**
     * The parent temporary directory for Zenith Core. (~/.weave/Zenith-Core/tmp)
     */
    val parentTempDirectory: File by lazy {
        File(HOME, ".weave/Zenith-Core/tmp")
            .also {
                if (!it.exists()) {
                    it.mkdirs()
                }
            }
    }

    /**
     * The temporary directory for the current instance of the game. ([parentTempDirectory]/[loadedTimestamp])
     */
    val tempDirectory: File by lazy {
        File(parentTempDirectory, loadedTimestamp.toString())
            .also {
                if (!it.exists()) {
                    it.mkdirs()
                }
            }
    }


    /**
     * Creates a temporary file in the temporary directory.
     *
     * @param prefix The prefix of the file name.
     * @param suffix The suffix of the file name.
     * @param randomCount The length of the random string in the file name. (1-30)
     * @return The [File] object of the temporary file which has not been created yet.
     */
    fun createTempFile(prefix: String = "", suffix: String = "", randomCount: Short = 10): File {
        require(randomCount in 1..30) { "randomCount must be between 1 and 30" }

        var file: File

        val random = StringBuilder()

        while (true) {
            for (i in 0 until randomCount) {
                random.append(RANDOM_STRING.random())
            }

            file = File(tempDirectory, "$prefix$random$suffix")

            if (!file.exists()) {
                break
            }

            random.clear()
        }

        return file
    }

    /**
     * Deletes all the files in the temporary directory.
     *
     * This method is called automatically when the game exits.
     */
    fun deleteTempDirectory() {
        tempDirectory.deleteRecursively()
    }

    /**
     * Deletes all the temporary directories in the parent temporary directory whose last modified time is older than [TEMPORARY_DIRECTORY_AGE].
     *
     * This method is called automatically when the game starts.
     */
    fun cleanParentTempDirectory() {
        val currentTime = System.currentTimeMillis()
        parentTempDirectory
            .listFiles()
            ?.filter { currentTime - it.lastModified() > TEMPORARY_DIRECTORY_AGE }
            ?.forEach(File::deleteRecursively)
    }
}