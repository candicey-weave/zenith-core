package com.gitlab.candicey.zenithcore.helper

import com.gitlab.candicey.zenithcore.GSON
import com.google.gson.annotations.SerializedName
import java.io.ByteArrayOutputStream
import java.io.File
import java.net.URL
import java.net.URLEncoder
import java.util.*
import javax.imageio.ImageIO

/**
 * A helper class for uploading images to imgur.
 */
object ImgurHelper {
    val imgurApi = URL("https://api.imgur.com/3/image")

    /**
     * Uploads an image to imgur.
     *
     * @param file The image to upload.
     * @return The response from imgur.
     */
    fun upload(file: File): ImgurResponse {
        val bufferedImage = ImageIO.read(file)
        val byteArrayOutputStream = ByteArrayOutputStream()
        ImageIO.write(bufferedImage, "png", byteArrayOutputStream)

        val data = "${URLEncoder.encode("image", "UTF-8")}=" +
                URLEncoder.encode(Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray()), "UTF-8") +
                "&" +
                URLEncoder.encode("key", "UTF-8") +
                "=" +
                URLEncoder.encode("68f6709591c60ccd7e54fbcb23c01695ee0dd179", "UTF-8")

        val urlConnection = imgurApi.openConnection().apply {
            doOutput = true
            doInput = true
            setRequestProperty("Authorization", "Client-ID 3195c1089c41047")
            setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
        }

        urlConnection
            .getOutputStream()
            .run {
                write(data.toByteArray())
                flush()
                close()
            }

        val read = urlConnection.getInputStream().readBytes().toString(Charsets.UTF_8)

        return GSON.fromJson(read, ImgurResponse::class.java)
    }

    data class ImgurResponse(
        val `data`: ImgurResponseData,
        val status: Int,
        val success: Boolean
    ) {
        data class ImgurResponseData(
            @SerializedName("account_id")
            val accountId: Int,
            @SerializedName("account_url")
            val accountUrl: Any?,
            val id: String,
            val title: Any?,
            @SerializedName("ad_type")
            val adType: Int,
            @SerializedName("ad_url")
            val adUrl: String,
            val animated: Boolean,
            val bandwidth: Int,
            val datetime: Int,
            @SerializedName("deletehash")
            val deleteHash: String,
            val description: Any?,
            val edited: String,
            val favorite: Boolean,
            @SerializedName("has_sound")
            val hasSound: Boolean,
            val height: Int,
            val width: Int,
            @SerializedName("in_gallery")
            val inGallery: Boolean,
            @SerializedName("in_most_viral")
            val inMostViral: Boolean,
            @SerializedName("is_ad")
            val isAd: Boolean,
            val link: String,
            val name: String,
            val nsfw: Any?,
            val section: Any?,
            val size: Int,
            val tags: List<Any>,
            val type: String,
            val views: Int,
            val vote: Any?
        ) {
            fun getUrl() = "https://i.imgur.com/$id.png"
        }
    }
}