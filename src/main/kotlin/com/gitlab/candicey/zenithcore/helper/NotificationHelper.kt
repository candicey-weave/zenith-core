package com.gitlab.candicey.zenithcore.helper

import com.gitlab.candicey.zenithcore.warn
import java.awt.Image
import java.awt.SystemTray
import java.awt.Toolkit
import java.awt.TrayIcon
import java.awt.event.ActionListener
import java.awt.event.MouseListener
import java.awt.event.MouseMotionListener

object NotificationHelper {
    private val systemTray: SystemTray? by lazy {
        if (!SystemTray.isSupported()) {
            warn("[NotificationHelper] SystemTray is not supported on this platform.")
            return@lazy null
        }

        SystemTray.getSystemTray()
    }

    fun showNotification(title: String, message: String? = null, image: Image? = Icon.INFO.image, actionListener: ActionListener? = null, mouseListener: MouseListener? = null, mouseMotionListener: MouseMotionListener? = null) {
        systemTray?.let {
            val trayIcon = TrayIcon(image, title)
            message?.let(trayIcon::setToolTip)
            trayIcon.isImageAutoSize = true
            actionListener?.let(trayIcon::addActionListener)
            mouseListener?.let(trayIcon::addMouseListener)
            mouseMotionListener?.let(trayIcon::addMouseMotionListener)

            it.add(trayIcon)
        }
    }

    enum class Icon(val image: Image?) {
        INFO(createImage("info")),
        WARNING(createImage("warning")),
        SUCCESS(createImage("success")),
        ERROR(createImage("error")),
    }

    private fun createImage(path: String): Image? =
        javaClass.getResource("/assets/lunar/icons/notification/$path-24x24.png")?.let(Toolkit.getDefaultToolkit()::createImage)
}