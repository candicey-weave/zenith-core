package com.gitlab.candicey.zenithcore.util

/**
 * A simple cache util. It will remove the oldest entry if the cache size is larger than [maxCacheSize] or the cache time is larger than [cacheTime].
 *
 * @param T The type of the key.
 * @param U The type of the value.
 * @property cacheTime The cache time in milliseconds. Default is 3 minutes.
 * @property maxCacheSize The max cache size. Default is 300.
 */
class Cache<T, U>(
    val cacheTime: Long = 3 * 60 * 1000, // 3 minutes
    val maxCacheSize: Int = 300,
) {
    /**
     * The cache map.
     * The key is the key of the cache.
     * The value is a pair of the time when the cache is stored and the value of the cache.
     */
    private val cache = object : LinkedHashMap<T, Pair<Long, U>>(maxCacheSize * 10 / 7, 0.7f, true) {
        override fun removeEldestEntry(eldest: MutableMap.MutableEntry<T, Pair<Long, U>>?): Boolean {
            return size > maxCacheSize
        }
    }

    /**
     * Stores a value to the cache.
     *
     * @param key The key of the cache.
     * @param value The value of the cache.
     */
    fun store(key: T, value: U) {
        update()
        cache[key] = System.currentTimeMillis() to value
    }

    /**
     * Gets a value from the cache.
     *
     * @param key The key of the cache.
     * @return The value of the cache. If the cache is not found, return null.
     */
    fun get(key: T): U? {
        update()
        return cache[key]?.second
    }

    /**
     * Gets a value from the cache. If the cache is not found, put the value to the cache.
     *
     * @param key The key of the cache.
     * @param value The value to put to the cache if the cache is not found.
     * @return The value of the cache.
     */
    fun getOrPut(key: T, value: () -> U): U {
        update()
        return cache[key]?.second ?: value().also { store(key, it) }
    }

    /**
     * Gets all the cache.
     *
     * @return A map of the cache.
     */
    fun getAll(): Map<T, U> {
        update()
        return cache.mapValues { it.value.second }
    }

    /**
     * Removes a cache.
     *
     * @param key The key of the cache.
     */
    fun remove(key: T) {
        cache.remove(key)
    }

    /**
     * Clears all the cache.
     */
    fun clear() {
        cache.clear()
    }

    /**
     * Updates the cache.
     */
    private fun update() {
        cache.entries.removeIf { it.value.first + cacheTime < System.currentTimeMillis() }
    }
}