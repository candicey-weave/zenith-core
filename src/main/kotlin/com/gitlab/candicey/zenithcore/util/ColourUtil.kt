package com.gitlab.candicey.zenithcore.util

import kotlin.math.sin

object ColourUtil {
    /**
     * Returns a rainbow colour based on the current time.
     *
     * @param speed The speed of the rainbow.
     * @param offset The offset of the rainbow.
     * @param alpha The alpha of the rainbow. (0-255)
     * @return The rainbow colour in ARGB.
     */
    fun rainbowColour(speed: Double = 1.0, offset: Double = 0.0, alpha: Int = 255): Int {
        val r = (sin(speed / 500 * System.currentTimeMillis() + offset) * 127 + 128).toInt().coerceIn(0, 255)
        val g = (sin(speed / 500 * System.currentTimeMillis() + offset + 2) * 127 + 128).toInt().coerceIn(0, 255)
        val b = (sin(speed / 500 * System.currentTimeMillis() + offset + 4) * 127 + 128).toInt().coerceIn(0, 255)

        return (alpha shl 24) or (r shl 16) or (g shl 8) or b
    }
}