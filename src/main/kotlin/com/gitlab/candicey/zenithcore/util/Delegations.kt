package com.gitlab.candicey.zenithcore.util

import com.gitlab.candicey.zenithcore.SYSTEM_PROPERTIES
import kotlin.reflect.KProperty

class DelegateHolder<T>(private var `object`: T) {
    operator fun getValue(instance: Any, kProperty: KProperty<*>): T = `object`

    operator fun setValue(instance: Any, kProperty: KProperty<*>, value: T) {
        `object` = value
    }

    operator fun getValue(nothing: Nothing?, kProperty: KProperty<*>): T = `object`

    operator fun setValue(nothing: Nothing?, kProperty: KProperty<*>, value: T) {
        `object` = value
    }
}

/**
 * A delegate that allows you to delegate to a System property.
 *
 * This delegate uses the `System.getProperties` method to retrieve the value of a property.
 * The property name is determined by the variable name where this delegate is used.
 *
 * If the property does not exist, it will be created with the default value provided.
 */
class PropertyDelegate<T>(private val property: String? = null, private val defaultValue: (() -> T)? = null) {
    operator fun getValue(instance: Any, kProperty: KProperty<*>): T {
        val propertyName = property ?: kProperty.name

        return SYSTEM_PROPERTIES.getOrPut(propertyName) { (defaultValue ?: error("Property $propertyName does not exist, and no default value was provided.")).invoke() } as T
    }

    operator fun getValue(instance: Nothing?, kProperty: KProperty<*>): T {
        val propertyName = property ?: kProperty.name

        return SYSTEM_PROPERTIES.getOrPut(propertyName) { (defaultValue ?: error("Property $propertyName does not exist, and no default value was provided.")).invoke() } as T
    }

    operator fun setValue(instance: Any, kProperty: KProperty<*>, value: T) {
        val propertyName = property ?: kProperty.name

        SYSTEM_PROPERTIES[propertyName] = value
    }

    operator fun setValue(instance: Nothing?, kProperty: KProperty<*>, value: T) {
        val propertyName = property ?: kProperty.name

        SYSTEM_PROPERTIES[propertyName] = value
    }
}

/**
 * A delegate that allows you to access private fields.
 */
class ShadowField<T>(private val ownerClass: Class<*>? = null, private val variableName: String? = null, private val instance: Any? = null) {
    private var fieldCache: FieldCache? = null

    operator fun getValue(instance: Any, kProperty: KProperty<*>): T {
        val fieldOwner = ownerClass ?: instance::class.java
        val fieldName = variableName ?: kProperty.name
        val fieldInstance = this.instance ?: instance

        return getFieldCache(fieldOwner, fieldName)[fieldInstance] as T
    }

    operator fun getValue(instance: Nothing?, kProperty: KProperty<*>): T {
        val fieldOwner = ownerClass ?: error("Field owner is not set")
        val fieldName = variableName ?: kProperty.name
        val fieldInstance = this.instance ?: error("Field instance is not set")

        return getFieldCache(fieldOwner, fieldName)[fieldInstance] as T
    }

    operator fun setValue(instance: Any, kProperty: KProperty<*>, value: T) {
        val fieldOwner = ownerClass ?: instance
        val fieldName = variableName ?: kProperty.name
        val fieldInstance = this.instance ?: instance

        getFieldCache(fieldOwner::class.java, fieldName)[fieldInstance] = value
    }

    operator fun setValue(instance: Nothing?, kProperty: KProperty<*>, value: T) {
        val fieldOwner = ownerClass ?: error("Field owner is not set")
        val fieldName = variableName ?: kProperty.name
        val fieldInstance = this.instance ?: error("Field instance is not set")

        getFieldCache(fieldOwner, fieldName)[fieldInstance] = value
    }

    private fun getFieldCache(ownerClass: Class<*>, fieldName: String): FieldCache {
        if (fieldCache == null) {
            fieldCache = FieldCache(ownerClass, fieldName)
        }
        
        return fieldCache!!
    }
}