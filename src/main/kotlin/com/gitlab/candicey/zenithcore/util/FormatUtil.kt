package com.gitlab.candicey.zenithcore.util

const val RIGHT_ARROW = "»"

const val SECTION_SIGN = "§"

const val BLACK = SECTION_SIGN + "0"
const val DARK_BLUE = SECTION_SIGN + "1"
const val DARK_GREEN = SECTION_SIGN + "2"
const val DARK_AQUA = SECTION_SIGN + "3"
const val DARK_RED = SECTION_SIGN + "4"
const val DARK_PURPLE = SECTION_SIGN + "5"
const val GOLD = SECTION_SIGN + "6"
const val GREY = SECTION_SIGN + "7"
const val DARK_GREY = SECTION_SIGN + "8"
const val BLUE = SECTION_SIGN + "9"
const val GREEN = SECTION_SIGN + "a"
const val AQUA = SECTION_SIGN + "b"
const val RED = SECTION_SIGN + "c"
const val LIGHT_PURPLE = SECTION_SIGN + "d"
const val YELLOW = SECTION_SIGN + "e"
const val WHITE = SECTION_SIGN + "f"

const val OBFUSCATED = SECTION_SIGN + "k"
const val BOLD = SECTION_SIGN + "l"
const val STRIKETHROUGH = SECTION_SIGN + "m"
const val UNDERLINE = SECTION_SIGN + "n"
const val ITALIC = SECTION_SIGN + "o"
const val RESET = SECTION_SIGN + "r"