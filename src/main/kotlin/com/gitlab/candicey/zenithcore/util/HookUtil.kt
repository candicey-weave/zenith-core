package com.gitlab.candicey.zenithcore.util

import com.gitlab.candicey.zenithcore.data.TypeDescriptor
import com.gitlab.candicey.zenithcore.enum.InjectPosition
import com.gitlab.candicey.zenithcore.extension.isStatic
import com.gitlab.candicey.zenithcore.extension.load
import com.gitlab.candicey.zenithcore.extension.parametersString
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import com.gitlab.candicey.zenithcore.extension.insertBeforeReturn
import net.weavemc.internals.asm
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.InsnList
import org.objectweb.asm.tree.LabelNode
import org.objectweb.asm.tree.MethodNode
import java.util.*

/**
 * Adds "on" in front of the method name if there is no "on" in front of the method name (camel case).
 *
 * For example, "onUpdate" will be "onUpdate", but "update" will be "onUpdate".
 */
private fun getCallingMethodName(methodName: String) =
    if (methodName.startsWith("on") && methodName[2].isUpperCase()) methodName
    else "on${
        methodName.replaceFirstChar {
            if (it.isLowerCase()) it.titlecase(Locale.getDefault())
            else it.toString()
        }
    }"

/**
 * Injects calls to static methods.
 *
 * The called methods' names are the inject method names with "on" in front of them.
 *
 * @param T The type of the class that contains the method to call.
 * @param injectMethodName The list of method names to inject into.
 * @param at The position to inject at.
 * @see callStatic
 */
inline fun <reified T : Any> ClassNode.callStatics(
    vararg injectMethodName: String,
    at: InjectPosition = InjectPosition.HEAD
) = callStatics(internalNameOf<T>(), injectMethodName, at)

/**
 * Injects calls to static methods.
 *
 * The called methods' names are the inject method names with "on" in front of them.
 *
 * @param internalName The internal name of the class that contains the method to call. (e.g. "net/minecraft/client/Minecraft")
 * @param injectMethodName The list of method names to inject into.
 * @param at The position to inject at.
 * @see callStatic
 */
fun ClassNode.callStatics(
    internalName: String,
    injectMethodName: Array<out String>,
    at: InjectPosition
) = injectMethodName.forEach { methodName ->
    methods
        .named(methodName)
        .callStatic(internalName, getCallingMethodName(methodName), name ?: error("Class name is null"), at)
}

/**
 * Injects calls to static methods and return if the called method returns true.
 *
 * The called methods' names are the inject method names with "on" in front of them.
 *
 * @param T The type of the class that contains the method to call.
 * @param injectMethodName The list of method names to inject into.
 * @param at The position to inject at.
 * @see callStaticBooleanIf
 */
inline fun <reified T : Any> ClassNode.callStaticsBooleanIf(
    vararg injectMethodName: String,
    at: InjectPosition = InjectPosition.HEAD
) = callStaticsBooleanIf(internalNameOf<T>(), injectMethodName, at)

/**
 * Injects calls to static methods and return if the called method returns true.
 *
 * The called methods' names are the inject method names with "on" in front of them.
 *
 * @param internalName The internal name of the class that contains the method to call. (e.g. "net/minecraft/client/Minecraft")
 * @param injectMethodName The list of method names to inject into.
 * @param at The position to inject at.
 * @see callStaticBooleanIf
 */
fun ClassNode.callStaticsBooleanIf(
    internalName: String,
    injectMethodName: Array<out String>,
    at: InjectPosition
) = injectMethodName.forEach { methodName ->
    methods
        .named(methodName)
        .callStaticBooleanIf(internalName, getCallingMethodName(methodName), name ?: error("Class name is null"), at)
}

/**
 * Injects calls to static methods and return if the called method returns true.
 *
 * The called methods' names are the inject method names with "on" in front of them.
 *
 * @param T The type of the class that contains the method to call.
 * @param injectMethod The list of Pair<methodToInjectInto, returnType> to inject.
 * @param at The position to inject at.
 * @see callStaticNotNull
 */
inline fun <reified T : Any> ClassNode.callStaticsNotNull(
    vararg injectMethod: Pair<String, String>,
    at: InjectPosition = InjectPosition.HEAD
) = callStaticsNotNull(internalNameOf<T>(), injectMethod, at)

/**
 * Injects calls to static methods and return if the called method returns true.
 *
 * The called methods' names are the inject method names with "on" in front of them.
 *
 * @param internalName The internal name of the class that contains the method to call. (e.g. "net/minecraft/client/Minecraft")
 * @param injectMethod The list of Pair<methodToInjectInto, returnType> to inject.
 * @param at The position to inject at.
 * @see callStaticNotNull
 */
fun ClassNode.callStaticsNotNull(
    internalName: String,
    injectMethod: Array<out Pair<String, String>>,
    at: InjectPosition
) = injectMethod.forEach { (methodName, returnType) ->
    methods
        .named(methodName)
        .callStaticNotNull(internalName, getCallingMethodName(methodName), returnType, name ?: error("Class name is null"), at)
}

/**
 * Injects calls to static methods.
 *
 * @param T The type of the class that contains the method to call.
 * @param data The list of Triple<callMethodName, injectMethodName, injectPosition> to inject.
 * @see callStatic
 */
inline fun <reified T : Any> ClassNode.callStatics(
    data: List<Triple<String, String, InjectPosition>>
) = callStatics(internalNameOf<T>(), data)

/**
 * Injects calls to static methods.
 *
 * @param internalName The internal name of the class that contains the method to call. (e.g. "net/minecraft/client/Minecraft")
 * @param data The list of Triple<callMethodName, injectMethodName, injectPosition> to inject.
 * @see callStatic
 */
fun ClassNode.callStatics(
    internalName: String,
    data: List<Triple<String, String, InjectPosition>>
) = data.forEach { (callMethodName, injectMethodName, at) ->
    methods
        .named(injectMethodName)
        .callStatic(internalName, callMethodName, name ?: error("Class name is null"), at)
}

/**
 * Injects a call to a static method and return if the called method returns true.
 *
 * @param T The type of the class that contains the method to call.
 * @param data The list of Triple<callMethodName, injectMethodName, injectPosition> to inject.
 * @see callStaticBooleanIf
 */
inline fun <reified T : Any> ClassNode.callStaticsBooleanIf(
    data: List<Triple<String, String, InjectPosition>>
) = callStaticsBooleanIf(internalNameOf<T>(), data)

/**
 * Injects a call to a static method and return if the called method returns true.
 *
 * @param internalName The internal name of the class that contains the method to call. (e.g. "net/minecraft/client/Minecraft")
 * @param data The list of Triple<callMethodName, injectMethodName, injectPosition> to inject.
 * @see callStaticBooleanIf
 */
fun ClassNode.callStaticsBooleanIf(
    internalName: String,
    data: List<Triple<String, String, InjectPosition>>
) = data.forEach { (callMethodName, injectMethodName, at) ->
    methods
        .named(injectMethodName)
        .callStaticBooleanIf(internalName, callMethodName, name ?: error("Class name is null"), at)
}

/**
 * Injects a static method call and returns the called method's return value if it is not null.
 *
 * @param T The type of the class that contains the method to call.
 * @param data The list of Triple<Pair<callMethodName, callMethodReturnType>, baseClassName, injectPosition> to inject.
 * @see callStaticNotNull
 */
inline fun <reified T : Any> ClassNode.callStaticsNotNull(
    data: List<Triple<Pair<String, String>, String, InjectPosition>>
) = callStaticsNotNull(internalNameOf<T>(), data)

/**
 * Injects a static method call and returns the called method's return value if it is not null.
 *
 * @param internalName The internal name of the class that contains the method to call. (e.g. "net/minecraft/client/Minecraft")
 * @param data The list of Triple<Pair<callMethodName, callMethodReturnType>, baseClassName, injectPosition> to inject.
 * @see callStaticNotNull
 */
fun ClassNode.callStaticsNotNull(
    internalName: String,
    data: List<Triple<Pair<String, String>, String, InjectPosition>>
) = data.forEach { (callMethod, injectMethodName, at) ->
    methods
        .named(injectMethodName)
        .callStaticNotNull(internalName, callMethod.first, callMethod.second, name ?: error("Class name is null"), at)
}

/**
 * Injects a call to a static method.
 *
 * @param T The type of the class that contains the method to call.
 * @param callMethodName The method name of the method to call.
 * @param injectMethodName The name of the method to inject the call.
 * @param at The position to inject the call.
 */
inline fun <reified T : Any> ClassNode.callStatic(
    callMethodName: String,
    injectMethodName: String,
    at: InjectPosition = InjectPosition.HEAD
) = callStatic(internalNameOf<T>(), callMethodName, injectMethodName, at)

/**
 * Injects a call to a static method.
 *
 * @param T The type of the class that contains the method to call.
 * @param callMethodName The method name of the method to call.
 * @param injectMethodMatcher The matcher used to find the method to inject the call.
 * @param at The position to inject the call.
 */
inline fun <reified T : Any> ClassNode.callStatic(
    callMethodName: String,
    noinline injectMethodMatcher: (MethodNode) -> Boolean,
    at: InjectPosition = InjectPosition.HEAD
) = callStatic(internalNameOf<T>(), callMethodName, injectMethodMatcher, at)

/**
 * Injects a call to a static method.
 *
 * @param callClassName The class name of the class that contains the method to call.
 * @param callMethodName The method name of the method to call.
 * @param injectMethodName The name of the method to inject the call.
 * @param at The position to inject the call.
 */
fun ClassNode.callStatic(
    callClassName: String,
    callMethodName: String,
    injectMethodName: String,
    at: InjectPosition = InjectPosition.HEAD
) = methods
    .named(injectMethodName)
    .callStatic(callClassName, callMethodName, name ?: error("Class name is null"), at)

/**
 * Injects a call to a static method.
 *
 * @param callClassName The class name of the class that contains the method to call.
 * @param callMethodName The method name of the method to call.
 * @param injectMethodMatcher The matcher used to find the method to inject the call.
 * @param at The position to inject the call.
 */
fun ClassNode.callStatic(
    callClassName: String,
    callMethodName: String,
    injectMethodMatcher: (MethodNode) -> Boolean,
    at: InjectPosition = InjectPosition.HEAD
) = methods
    .find { injectMethodMatcher.invoke(it) }
    ?.callStatic(callClassName, callMethodName, name ?: error("Class name is null"), at)

/**
 * Injects a call to a static method.
 *
 * @param T The type of the class that contains the method to call.
 * @param callMethodName The method name of the method to call.
 * @param baseClassName The class name of the method that is being injected.
 * @param at The position to inject the call.
 */
inline fun <reified T : Any> MethodNode.callStatic(
    callMethodName: String,
    baseClassName: String,
    at: InjectPosition = InjectPosition.HEAD
) = callStatic(internalNameOf<T>(), callMethodName, baseClassName, at)

/**
 * Injects a call to a static method and return if the called method returns true.
 *
 * @param T The type of the class that contains the method to call.
 * @param callMethodName The method name of the method to call.
 * @param baseClassName The class name of the method that is being injected.
 * @param at The position to inject the call.
 */
inline fun <reified T : Any> MethodNode.callStaticBooleanIf(
    callMethodName: String,
    baseClassName: String,
    at: InjectPosition = InjectPosition.HEAD
) = callStaticBooleanIf(internalNameOf<T>(), callMethodName, baseClassName, at)

/**
 * Injects a call to a static method.
 *
 * @param T The type of the class that contains the method to call.
 * @param callMethodName The method name of the method to call.
 * @param callMethodReturnType The return type of the method to call.
 * @param baseClassName The class name of the method that is being injected.
 * @param at The position to inject the call.
 */
inline fun <reified T : Any> MethodNode.callStaticNotNull(
    callMethodName: String,
    callMethodReturnType: String,
    baseClassName: String,
    at: InjectPosition = InjectPosition.HEAD
) = callStaticNotNull(internalNameOf<T>(), callMethodName, callMethodReturnType, baseClassName, at)

private fun List<String>.foldLoadParameters(initialIndex: Int): InsnList =
    asm {
        fold(initialIndex) { acc, typeDescriptorString ->
            val typeDescriptor = TypeDescriptor(typeDescriptorString)
            load(acc, typeDescriptor)
            acc + typeDescriptor.stackSize
        }
    }

/**
 * Injects a call to a static method.
 *
 * @param callClassName The class name of the method to call.
 * @param callMethodName The method name of the method to call.
 * @param baseClassName The class name of the method that is being injected.
 * @param at The position to inject the call.
 */
fun MethodNode.callStatic(
    callClassName: String,
    callMethodName: String,
    baseClassName: String,
    at: InjectPosition = InjectPosition.HEAD
) {
    val asm = asm {
        if (isStatic) {
            parametersString?.foldLoadParameters(0)?.unaryPlus()
        } else {
            aload(0)
            parametersString?.foldLoadParameters(1)?.unaryPlus()
        }

        invokestatic(callClassName, callMethodName, "(L$baseClassName;${desc.substring(1).substringBefore(")")})V")
    }

    when (at) {
        InjectPosition.HEAD -> instructions.insert(asm)
        InjectPosition.RETURN -> instructions.insertBeforeReturn(asm)
    }
}

/**
 * Injects a call to a static method and return if the called method returns true.
 *
 * @param callClassName The class name of the method to call.
 * @param callMethodName The method name of the method to call.
 * @param baseClassName The class name of the method that is being injected.
 * @param at The position to inject the call.
 */
fun MethodNode.callStaticBooleanIf(
    callClassName: String,
    callMethodName: String,
    baseClassName: String,
    at: InjectPosition = InjectPosition.HEAD
) {
    val asm = asm {
        if (isStatic) {
            parametersString?.foldLoadParameters(0)?.unaryPlus()
        } else {
            aload(0)
            parametersString?.foldLoadParameters(1)?.unaryPlus()
        }

        val label = LabelNode()

        invokestatic(callClassName, callMethodName, "(L$baseClassName;${desc.substring(1).substringBefore(")")})Z")
        ifeq(label)
        _return
        +label
    }

    when (at) {
        InjectPosition.HEAD -> instructions.insert(asm)
        InjectPosition.RETURN -> instructions.insertBeforeReturn(asm)
    }
}

/**
 * Injects a static method call and returns the called method's return value if it is not null.
 *
 * @param callClassName The class name where the method to call is in.
 * @param callMethodName The method name of the method to call.
 * @param callMethodReturnType The return type of the method to call.
 * @param baseClassName The class name of the method that is being injected.
 * @param at The position to inject the call.
 */
fun MethodNode.callStaticNotNull(
    callClassName: String,
    callMethodName: String,
    callMethodReturnType: String,
    baseClassName: String,
    at: InjectPosition = InjectPosition.HEAD
) {
    val asm = asm {
        if (isStatic) {
            parametersString?.foldLoadParameters(0)?.unaryPlus()
        } else {
            aload(0)
            parametersString?.foldLoadParameters(1)?.unaryPlus()
        }

        val label = LabelNode()

        invokestatic(callClassName, callMethodName, "(L$baseClassName;${desc.substring(1).substringBefore(")")})$callMethodReturnType")
        dup
        ifnull(label)
        areturn
        +label
        pop
    }

    when (at) {
        InjectPosition.HEAD -> instructions.insert(asm)
        InjectPosition.RETURN -> instructions.insertBeforeReturn(asm)
    }
}