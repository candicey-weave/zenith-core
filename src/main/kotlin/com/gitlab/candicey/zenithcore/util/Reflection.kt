package com.gitlab.candicey.zenithcore.util

import com.gitlab.candicey.zenithcore.extension.descriptor
import com.gitlab.candicey.zenithcore.extension.getAllField
import com.gitlab.candicey.zenithcore.extension.getAllMethod
import com.gitlab.candicey.zenithcore.mapClass
import com.gitlab.candicey.zenithcore.unmapClass
import com.gitlab.candicey.zenithcore.versionCompatibilityAdaptor

/**
 * A map of class names to [Class] objects.
 */
private val classCache = mutableMapOf<String, Class<*>>()

/**
 * A map of method keys to [MethodCache] objects.
 *
 * @see MethodCache.generateMethodKey
 */
private val methodCache = mutableMapOf<String, MethodCache>()

/**
 * A map of field keys to [FieldCache] objects.
 *
 * @see FieldCache.generateFieldKey
 */
private val fieldCache = mutableMapOf<String, FieldCache>()

/**
 * Reflects a class and caches it for faster access later.
 *
 * @param className The name of the class to reflect. (e.g. "com.gitlab.candicey.zenithcore.util.ReflectionUtil" or "com/gitlab/candicey/zenithcore/util/ReflectionUtil")
 * @return The [Class] object.
 */
fun reflectClass(className: String): Class<*> =
    classCache[className] ?: Class.forName(mapClass(className.replace('/', '.'))).also { classCache[className] = it }

/**
 * Reflects a method in the current class and caches it for faster access later.
 *
 * @param methodName The name of the method to reflect.
 * @param args The arguments of the method.
 * @param returnType The return type of the method.
 * @return The [MethodCache] object.
 */
fun Any.reflectMethod(methodName: String, vararg args: Class<*>, returnType: Class<*> = Void::class.javaPrimitiveType as Class<*>): MethodCache =
    MethodCache.find(this::class.java, methodName, *args, returnType = returnType)

/**
 * Reflects a method in the specified class and caches it for faster access later.
 *
 * @param ownerClass The class that owns the method.
 * @param methodName The name of the method to reflect.
 * @param args The arguments of the method.
 * @return The [MethodCache] object.
 */
fun reflectMethod(ownerClass: Class<*>, methodName: String, vararg args: Class<*>, returnType: Class<*> = Void::class.javaPrimitiveType as Class<*>): MethodCache =
    MethodCache.find(ownerClass, methodName, *args, returnType = returnType)

/**
 * Reflects a method in the specified class and caches it for faster access later.
 *
 * @param ownerName The name of the class that owns the method. (e.g. "com.gitlab.candicey.zenithcore.util.ReflectionUtil" or "com/gitlab/candicey/zenithcore/util/ReflectionUtil")
 * @param methodName The name of the method to reflect.
 * @param args The arguments of the method.
 * @param returnType The return type of the method.
 * @return The [MethodCache] object.
 */
fun reflectMethod(ownerName: String, methodName: String, vararg args: Class<*>, returnType: Class<*> = Void::class.javaPrimitiveType as Class<*>): MethodCache =
    MethodCache.find(ownerName, methodName, *args, returnType = returnType)

/**
 * Reflects a method in the current class and caches it for faster access later.
 *
 * @param T The class that owns the method.
 * @param methodName The name of the method to reflect.
 * @param args The arguments of the method.
 * @param returnType The return type of the method.
 * @return The [MethodCache] object.
 */
inline fun <reified T> reflectMethod(methodName: String, vararg args: Class<*>, returnType: Class<*> = Void::class.javaPrimitiveType as Class<*>): MethodCache =
    reflectMethod(T::class.java, methodName, *args, returnType = returnType)

/**
 * Reflects a field in the current class and caches it for faster access later.
 *
 * @param fieldName The name of the field to reflect.
 * @return The [FieldCache] object.
 */
fun Any.reflectField(fieldName: String) =
    FieldCache.find(this::class.java, fieldName)

/**
 * Reflects a field in the specified class and caches it for faster access later.
 *
 * @param ownerClass The class that owns the field.
 * @param fieldName The name of the field to reflect.
 * @return The [FieldCache] object.
 */
fun reflectField(ownerClass: Class<*>, fieldName: String): FieldCache =
    FieldCache.find(ownerClass, fieldName)

/**
 * Reflects a field in the specified class and caches it for faster access later.
 *
 * @param ownerName The name of the class that owns the field. (e.g. "com.gitlab.candicey.zenithcore.util.ReflectionUtil" or "com/gitlab/candicey/zenithcore/util/ReflectionUtil")
 * @param fieldName The name of the field to reflect.
 * @return The [FieldCache] object.
 */
fun reflectField(ownerName: String, fieldName: String): FieldCache =
    FieldCache.find(ownerName, fieldName)

/**
 * Reflects a field in the current class and caches it for faster access later.
 *
 * @param T The class that owns the field.
 * @param fieldName The name of the field to reflect.
 * @return The [FieldCache] object.
 */
inline fun <reified T> reflectField(fieldName: String): FieldCache =
    reflectField(T::class.java, fieldName)

/**
 * A data class that is used to cache the method.
 *
 * @property ownerClass The class that owns the method.
 * @property methodName The name of the method.
 * @property args The arguments of the method.
 * @property returnType The return type of the method.
 */
data class MethodCache(val ownerClass: Class<*>, val methodName: String, val args: List<Class<*>>, val returnType: Class<*> = Void::class.javaPrimitiveType as Class<*>) {
    /**
     * The [java.lang.reflect.Method] object. It will be initialised lazily.
     */
    val method by lazy {
        val unmappedClass = unmapClass(ownerClass.name)
        val unmappedDesc = buildString {
            append('(')
            args.forEach { append(it.descriptor) }
            append(')')
            append(returnType.descriptor)
        }.let(versionCompatibilityAdaptor.`mappings$unmapper`::mapMethodDesc)

        val mappedMethodName = versionCompatibilityAdaptor.`mappings$mapper`.mapMethodName(unmappedClass, methodName, unmappedDesc)

        ownerClass
            .getAllMethod(mappedMethodName, *args.toTypedArray(), returnType = returnType)
            ?.also { it.isAccessible = true }
            ?: throw NoSuchMethodException("${ownerClass.name}.$methodName(${args.joinToString { it.descriptor }})${returnType.descriptor} => $unmappedClass.$methodName$unmappedDesc")
    }

    val key by lazy { generateMethodKey(ownerClass, methodName, args, returnType) }

    /**
     * Invoke the method.
     *
     * @param args The arguments of the method.
     */
    operator fun invoke(instance: Any? = null, vararg args: Any?): Any? = method(instance, *args)

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }

        if (other !is MethodCache) {
            return false
        }

        if (ownerClass != other.ownerClass) {
            return false
        }

        if (methodName != other.methodName) {
            return false
        }

        if (args != other.args) {
            return false
        }

        return returnType == other.returnType
    }

    override fun hashCode(): Int {
        var result = ownerClass.hashCode()
        result = 31 * result + methodName.hashCode()
        result = 31 * result + args.hashCode()
        result = 31 * result + returnType.hashCode()
        return result
    }

    companion object {
        fun generateMethodKey(ownerClass: Class<*>, methodName: String, args: List<Class<*>>, returnType: Class<*>): String =
            "${ownerClass.name}#$methodName(${args.joinToString(", ") { it.descriptor }})${returnType.descriptor}"

        /**
         * Find the method in the cache. If it is not found, create a new one.
         *
         * @param ownerClass The class that owns the method.
         * @param methodName The name of the method.
         * @param args The arguments of the method.
         * @param returnType The return type of the method.
         * @return The [MethodCache] object.
         */
        fun find(ownerClass: Class<*>, methodName: String, vararg args: Class<*>, returnType: Class<*> = Void::class.javaPrimitiveType as Class<*>): MethodCache =
            methodCache.getOrPut(generateMethodKey(ownerClass, methodName, args.toList(), returnType)) {
                MethodCache(
                    ownerClass,
                    methodName,
                    args.toList(),
                    returnType
                )
            }

        /**
         * Find the method in the cache. If it is not found, create a new one.
         *
         * @param ownerName The name of the class that owns the method.
         * @param methodName The name of the method.
         * @param args The arguments of the method.
         * @param returnType The return type of the method.
         * @return The [MethodCache] object.
         */
        fun find(ownerName: String, methodName: String, vararg args: Class<*>, returnType: Class<*> = Void::class.javaPrimitiveType as Class<*>): MethodCache =
            find(reflectClass(ownerName), methodName, *args, returnType = returnType)
    }
}

/**
 * A data class that is used to cache the field.
 *
 * @property ownerClass The class that owns the field.
 * @property fieldName The name of the field.
 */
data class FieldCache(val ownerClass: Class<*>, val fieldName: String) {
    /**
     * The [java.lang.reflect.Field] object. It will be initialised lazily.
     */
    val field by lazy {
        val unmappedClass = unmapClass(ownerClass.name)

        val mappedFieldName = versionCompatibilityAdaptor.`mappings$mapper`.mapFieldName(unmappedClass, fieldName, null)

        ownerClass
            .getAllField(mappedFieldName)
            ?.also { it.isAccessible = true }
            ?: throw NoSuchFieldException("$ownerClass.$fieldName => $unmappedClass.$mappedFieldName")
    }

    val key by lazy { generateFieldKey(ownerClass, fieldName) }

    /**
     * Get the value of the field.
     *
     * @param instance The instance of the class. If it is null, it will be a static field.
     * @return The value of the field.
     */
    operator fun get(instance: Any? = null): Any? = field.get(instance)

    /**
     * Set the value of the field.
     *
     * @param instance The instance of the class. If it is null, it will be a static field.
     * @param value The value of the field.
     */
    operator fun set(instance: Any? = null, value: Any?) = field.set(instance, value)

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }

        if (other !is FieldCache) {
            return false
        }

        if (ownerClass != other.ownerClass) {
            return false
        }

        return fieldName == other.fieldName
    }

    override fun hashCode(): Int {
        var result = ownerClass.hashCode()
        result = 31 * result + fieldName.hashCode()
        return result
    }

    companion object {
        fun generateFieldKey(ownerClass: Class<*>, fieldName: String): String =
            "${ownerClass.name}#$fieldName"

        /**
         * Find the field in the cache. If it is not found, create a new one.
         *
         * @param ownerClass The class that owns the field.
         * @param fieldName The name of the field.
         * @return The [FieldCache] object.
         */
        fun find(ownerClass: Class<*>, fieldName: String): FieldCache =
            fieldCache.getOrPut(generateFieldKey(ownerClass, fieldName)) { FieldCache(ownerClass, fieldName) }

        /**
         * Find the field in the cache. If it is not found, create a new one.
         *
         * @param ownerName The name of the class that owns the field.
         * @param fieldName The name of the field.
         * @return The [FieldCache] object.
         */
        fun find(ownerName: String, fieldName: String): FieldCache =
            find(reflectClass(ownerName), fieldName)
    }
}