@file:Suppress("unused", "MemberVisibilityCanBePrivate")

package com.gitlab.candicey.zenithcore.util

/**
 * A simple time counter.
 *
 * @param startTime Start time in milliseconds.
 * @param endTime End time in milliseconds.
 */
class TimeCount(val startTime: Long, val endTime: Long) {
    /**
     * @param seconds Time you want to count in seconds.
     */
    constructor(seconds: Int) : this(System.currentTimeMillis(), System.currentTimeMillis() + seconds * 1000)

    /**
     * Gets time left in milliseconds.
     */
    fun getTimeLeft() = endTime - System.currentTimeMillis()

    /**
     * Gets time passed in milliseconds.
     */
    fun getTimePassed() = System.currentTimeMillis() - startTime

    /**
     * Gets time left in percent. (0.0 ~ 1.0)
     */
    fun getTimeLeftPercent() = getTimeLeft() / endTime.toDouble()

    /**
     * Gets time passed in percent. (0.0 ~ 1.0)
     */
    fun getTimePassedPercent() = getTimePassed() / endTime.toDouble()

    /**
     * Gets the difference between start time and end time.
     */
    fun getDifference() = endTime - startTime

    fun isTimeLeft() = getTimeLeft() > 0

    fun isTimeOver() = !isTimeLeft()
}