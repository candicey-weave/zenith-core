plugins {
    kotlin("jvm") version "1.9.23"
    `java-library`
    `maven-publish`
    id("com.gitlab.candicey.stellar") version "0.2.0"
    id("net.weavemc.gradle") version "1.0.0-PRE"
}

val gitlabProjectId: String by project

stellar {
    relocate {
        relocateAsm()

        addClassReplacement(
            "com/gitlab/candicey/zenithcore",
            "com/gitlab/candicey/zenithcore_v${version.toString().replace('.', '_')}"
        )
    }
}

weave {
    configure {
        name = "Dummy"
        modId = "dummy"
        entryPoints = listOf("d.u.m.m.y.DummyMod")
        mixinConfigs = listOf("dummy.mixins.json")
        mcpMappings()
    }
    version("1.8.9")
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(mapOf("path" to ":")))
    implementation(libs.weaveLoader)
    implementation(libs.weaveCommon)
    implementation(libs.weaveInternals)
}

java {
    withSourcesJar()
    withJavadocJar()
}

kotlin {
    jvmToolchain(8)
}

tasks.test {
    useJUnitPlatform()
}

publishing {
    publications {
        create<MavenPublication>("library") {
            groupId = "${project.group}.versioned"
            artifactId = project.name

            from(components["java"])

            val libsDirectory = File(project.projectDir, "build/libs")

            val file = libsDirectory.resolve("${project.name}-${project.version}-relocated.jar")
            artifact(file) {
                classifier = "relocated"
            }

            val namespaceTxt = libsDirectory.resolve("namespace.txt")
            if (!namespaceTxt.exists()) {
                namespaceTxt.parentFile?.mkdirs()
                namespaceTxt.createNewFile()
                namespaceTxt.writeText("mcp-named")
            }
            artifact(namespaceTxt) {
                classifier = "namespace"
            }
        }
    }

    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = findProperty("gitLabPrivateToken") as String?
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}
