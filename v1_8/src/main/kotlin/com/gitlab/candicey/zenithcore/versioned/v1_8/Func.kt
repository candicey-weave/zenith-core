package com.gitlab.candicey.zenithcore.versioned.v1_8

import com.gitlab.candicey.zenithcore.ZENITH_CORE_VERSION

internal fun info(message: String) = LOGGER.info("[Zenith-Core API v1.8 (${ZENITH_CORE_VERSION})] $message")
internal fun warn(message: String) = LOGGER.warn("[Zenith-Core API v1.8 (${ZENITH_CORE_VERSION})] $message")