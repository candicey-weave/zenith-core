package com.gitlab.candicey.zenithcore.versioned.v1_8

import com.gitlab.candicey.zenithcore.ZENITH_CORE_VERSION
import com.gitlab.candicey.zenithcore.versioned.v1_8.helper.ScaledResolutionHelper
import net.minecraft.client.Minecraft
import net.minecraft.client.gui.FontRenderer
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger

internal val LOGGER: Logger by lazy { LogManager.getLogger("Zenith-Core v$ZENITH_CORE_VERSION") }

val mc: Minecraft
    get() = Minecraft.getMinecraft()

val fontRenderer: FontRenderer
    get() = mc.fontRendererObj
val displayWidth: Int
    get() = mc.displayWidth
val displayHeight: Int
    get() = mc.displayHeight
val gameWidth: Int
    get() = ScaledResolutionHelper.scaledResolution.scaledWidth
val gameHeight: Int
    get() = ScaledResolutionHelper.scaledResolution.scaledHeight