package com.gitlab.candicey.zenithcore.versioned.v1_8

import com.gitlab.candicey.zenithcore.ZENITH_CORE_VERSION
import com.gitlab.candicey.zenithcore.common.IVersionCompatibilityAdaptor
import com.grappenmaker.mappings.MappingsRemapper
import net.weavemc.loader.util.MappingsHandler

object VersionCompatibilityAdaptor : IVersionCompatibilityAdaptor by IVersionCompatibilityAdaptor {
    override val `mappings$namespace`: String = "mcp-named"
    override val `mappings$mapper`: MappingsRemapper = MappingsHandler.mapper(`mappings$namespace`, MappingsHandler.environmentNamespace)
    override val `mappings$unmapper`: MappingsRemapper = `mappings$mapper`.reverse()

    override fun `logger$info`(message: String) = LOGGER.info("[Zenith-Core ($ZENITH_CORE_VERSION)] $message")
    override fun `logger$warn`(message: String) = LOGGER.warn("[Zenith-Core ($ZENITH_CORE_VERSION)] $message")
}