package com.gitlab.candicey.zenithcore.versioned.v1_8

import com.gitlab.candicey.zenithcore.common.IVersionEntryPoint
import com.gitlab.candicey.zenithcore.extension.registerHook
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.StartGameEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.hook.EntityJoinWorldEventHook
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.hook.LateKeyboardEventHook
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.hook.PlayerTickEventHook
import com.gitlab.candicey.zenithcore.versioned.v1_8.font.FontManager
import com.gitlab.candicey.zenithcore.versioned.v1_8.helper.MessageHelper
import com.gitlab.candicey.zenithcore.versioned.v1_8.helper.ScaledResolutionHelper
import com.gitlab.candicey.zenithcore.versioned.v1_8.hook.*
import com.gitlab.candicey.zenithcore.versioned.v1_8.hook.event.*
import com.gitlab.candicey.zenithcore.versioned.v1_8.keybind.KeyBindManager
import net.weavemc.api.event.EventBus
import net.weavemc.loader.InjectionHandler

class VersionEntryPoint : IVersionEntryPoint {
    override val versionCompatibilityAdaptor = VersionCompatibilityAdaptor

    override fun init() {
        info("Registering hooks...")
        arrayOf(
            ChatReceivedEventHook,
            ChatSentEventHook,
            EntityListEventAddHook,
            EntityListEventRemoveHook,
            GuiOpenEventHook,
            KeyboardEventHook,
            MouseEventHook,
            PlayerListEventHook,
            RenderGameOverlayHook,
            RenderHandEventHook,
            RenderLivingEventHook,
            RenderWorldEventHook,
            ServerConnectEventHook,
            ShutdownEventHook,
            StartGameEventHook,
            TickEventHook,
            WorldEventHook,
            PacketEventHook,

            EntityJoinWorldEventHook,
            LateKeyboardEventHook,
            PlayerTickEventHook,

            GL11Hook,
            GuiScreenHook,
            GuiConnectingHook,
            LocaleHook,
            ChatComponentTextHook,
            ChatStyleHook,
            ClickEventHook,
            ClickEventActionHook,
        ).forEach(InjectionHandler::registerHook)
        info("Hooks registered!")

        info("Registering event listeners...")
        arrayOf(
            KeyBindManager,
            MessageHelper,
            ScaledResolutionHelper,
        ).forEach(EventBus::subscribe)
        info("Event listeners registered!")

        EventBus.subscribe(StartGameEvent.Post::class.java) {
            FontManager.waitUntilReady()
        }
    }
}