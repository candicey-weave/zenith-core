package com.gitlab.candicey.zenithcore.versioned.v1_8.child

import net.minecraft.client.Minecraft
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.GuiScreen
import net.minecraft.client.gui.GuiTextField

/**
 * A child of [GuiScreen] that removes Lunar Client's injection of the GuiScreen's drawScreen method.
 * And to remap parameters to the correct names.
 */
open class GuiScreenChild : GuiScreen() {
    private val guiTextFieldList = mutableListOf<GuiTextField>()

    override fun initGui() {
    }

    override fun drawScreen(mouseX: Int, mouseY: Int, partialTicks: Float) {
        buttonList.forEach { it.drawButton(mc, mouseX, mouseY) }
        labelList.forEach { it.drawLabel(mc, mouseX, mouseY) }
        guiTextFieldList.forEach { it.drawTextBox() }
    }

    override fun actionPerformed(button: GuiButton?) {
    }

    override fun mouseClicked(mouseX: Int, mouseY: Int, mouseButton: Int) {
        super.mouseClicked(mouseX, mouseY, mouseButton)
        guiTextFieldList.forEach { it.mouseClicked(mouseX, mouseY, mouseButton) }
    }

    override fun keyTyped(char: Char, keyCode: Int) {
        super.keyTyped(char, keyCode)
        guiTextFieldList.filter { it.isFocused }.forEach { it.textboxKeyTyped(char, keyCode) }
    }

    override fun setWorldAndResolution(mc: Minecraft, width: Int, height: Int) {
        guiTextFieldList.clear()
        super.setWorldAndResolution(mc, width, height)
    }

    fun addGuiTextField(vararg guiTextField: GuiTextField) {
        guiTextFieldList.addAll(guiTextField)
    }
}