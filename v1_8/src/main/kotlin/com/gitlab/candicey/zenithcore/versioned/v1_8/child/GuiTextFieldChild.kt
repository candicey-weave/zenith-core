package com.gitlab.candicey.zenithcore.versioned.v1_8.child

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.client.gui.FontRenderer
import net.minecraft.client.gui.GuiScreen
import net.minecraft.client.gui.GuiTextField
import org.lwjgl.input.Keyboard

open class GuiTextFieldChild(
    @get:JvmName("getIdChild")
    val id: Int,
    fontRenderer: FontRenderer,
    xPosition: Int,
    yPosition: Int,
    @get:JvmName("getWidthChild")
    val width: Int,
    val height: Int,
) : GuiTextField(
    id,
    fontRenderer,
    xPosition,
    yPosition,
    width,
    height,
) {
    /**
     * The key codes that are ignored by the GuiTextField.
     */
    open val ignoredKeyCodes = mutableListOf(Keyboard.KEY_BACK, Keyboard.KEY_HOME, Keyboard.KEY_LEFT, Keyboard.KEY_RIGHT, Keyboard.KEY_END, Keyboard.KEY_DELETE) // 14, 199, 203, 205, 207, 211

    /**
     * The matcher that is used to check if the typed character is allowed.
     *
     * The first parameter is the typed character and the second parameter is the key code.
     */
    open var allowedCharactersMatcher: (Char, Int) -> Boolean = { _, _ -> true }

    /**
     * The maximum length of the text.
     */
    @get:JvmName("getMaxStringLengthChild")
    @set:JvmName("setMaxStringLengthChild")
    var maxStringLength: Int by ShadowField(GuiTextField::class.java)

    override fun textboxKeyTyped(typedChar: Char, keyCode: Int): Boolean {
        if (!GuiScreen.isCtrlKeyDown() && !ignoredKeyCodes.contains(keyCode) && !allowedCharactersMatcher.invoke(typedChar, keyCode)) {
            return false
        }

        return super.textboxKeyTyped(typedChar, keyCode)
    }
}