package com.gitlab.candicey.zenithcore.versioned.v1_8.command

import com.gitlab.candicey.zenithcore.util.*
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent
import net.minecraft.event.ClickEvent
import net.minecraft.event.HoverEvent

/**
 * The super class of all commands.
 *
 * @property commandInitialisationData The data for initialising the command.
 * @see [CommandInitialisationData]
 * @see [CommandManager]
 */
abstract class CommandAbstract(private val commandInitialisationData: CommandInitialisationData) {
    /**
     * If true, the sub commands will be run.
     */
    var runSubCommand: Boolean = false

    /**
     * If true, the [postExecute] will be run.
     */
    var runPostExecute: Boolean = false

    /**
     * If true, the [preRunSubCommand] will be run.
     */
    var runPreSubCommand: Boolean = false

    /**
     * The list of sub commands.
     */
    open val subCommands: MutableList<CommandAbstract> = mutableListOf()

    /**
     * The info of this command.
     */
    var commandInfo: CommandInfo = this::class.java.getAnnotation(CommandInfo::class.java) ?: throw IllegalArgumentException("Command class must be annotated with @CommandInfo (class: ${javaClass.name})")

    /**
     * If true, this command will be hidden from the help message.
     */
    var hideCommand: Boolean = this::class.java.getAnnotation(HideCommand::class.java) != null

    /**
     * The aliases of this command.
     */
    var aliases: List<String> = commandInfo.aliases.toList()

    /**
     * The description of this command.
     */
    var description: String = commandInfo.description

    /**
     * Run before [execute] and managing all things in this command.
     *
     * @param args The arguments of the command.
     * @param rawMessage The raw message of the command.
     * @param previousCommandName A list of the previous command names.
     */
    fun preExecute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        runSubCommand = false
        runPostExecute = false
        runPreSubCommand = false

        execute(args, rawMessage, previousCommandName)

        if (runSubCommand) {
            val nextArgsList = args.drop(1)
            val nextCommandName = args[0]

            val filters = subCommands
                .filter { it.isThisCommand(nextCommandName) }
                .toMutableList()

            if (filters.isEmpty()) {
                commandInitialisationData
                    .addPrefixFunction("$RED${BOLD}Command not found!")
                    .toChatComponent()
                    .addChatMessage()
            } else {
                if (runPreSubCommand) {
                    preRunSubCommand(filters)
                }

                for (filter in filters) {
                    filter.preExecute(nextArgsList, rawMessage, previousCommandName + aliases[0])
                }
            }

            if (runPostExecute) {
                postExecute()
            }
        }
    }

    /**
     * Execute the command.
     *
     * @param args The arguments of the command.
     * @param rawMessage The raw message of the command.
     * @param previousCommandName A list of the previous command names.
     */
    open fun execute(args: List<String>, rawMessage: String, previousCommandName: List<String>) {
        if (args.isEmpty()) {
            sendHelpMessage(previousCommandName)
        } else {
            runSubCommand = true
        }
    }

    /**
     * Run after running sub commands.
     */
    open fun postExecute() {
    }

    /**
     * Run before running sub commands.
     *
     * @param subCommands The sub commands that will be run.
     */
    open fun preRunSubCommand(subCommands: MutableList<CommandAbstract>) {
    }

    /**
     * Check if the message matches this command.
     *
     * @param command The message to check.
     * @return If the message matches this command.
     */
    fun isThisCommand(command: String): Boolean = aliases.any { it.equals(command, true) }

    /**
     * Send help message to the player. All the commands that are not hidden will be displayed.
     *
     * @param previousCommandName All the previous command names.
     */
    open fun sendHelpMessage(previousCommandName: List<String>) {
        commandInitialisationData
            .addPrefixFunction("$YELLOW${BOLD}${UNDERLINE}Available commands$YELLOW${BOLD}:")
            .toChatComponent()
            .addChatMessage()

        subCommands.forEach { commandAbstract ->
            val prefix = commandInitialisationData.prefix
            val subcommandName = commandInitialisationData.commandName[0]
            val subcommandAlias = commandAbstract.aliases[0]
            if (!commandAbstract.hideCommand) {
                val iChatComponent = commandInitialisationData
                    .addPrefixFunction(" $GREY$BOLD$RIGHT_ARROW$RESET ")
                    .toChatComponent()

                val command = "$prefix${subcommandName}${if (previousCommandName.isNotEmpty()) " ${previousCommandName.joinToString(" ")}" else ""} ${aliases[0]} $subcommandAlias"
                val aliasComponent = "$DARK_AQUA$subcommandAlias".toChatComponent().apply {
                    chatStyle.apply {
                        chatClickEvent = ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, command)
                        chatHoverEvent = HoverEvent(HoverEvent.Action.SHOW_TEXT, command.toChatComponent())
                    }
                }
                iChatComponent.appendSibling(aliasComponent)

                if (commandAbstract.description.isNotBlank()) {
                    iChatComponent
                        .appendSibling(" $YELLOW- ${commandAbstract.description}".toChatComponent())
                }

                iChatComponent.addChatMessage()
            }
        }
    }

    /**
     * If the [args] is empty, send help message.
     *
     * @param args The arguments of the command.
     * @param previousCommandName All the previous command names.
     */
    open fun checkArgs(args: List<String>, previousCommandName: List<String>) {
        if (args.isEmpty()) {
            sendHelpMessage(previousCommandName)
        }
    }
}