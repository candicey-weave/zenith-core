package com.gitlab.candicey.zenithcore.versioned.v1_8.command

/**
 * Used to store the info of a command.
 *
 * @property aliases The aliases of the command.
 * @property description The description of the command.
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class CommandInfo(
    vararg val aliases: String,
    val description: String = "",
)
