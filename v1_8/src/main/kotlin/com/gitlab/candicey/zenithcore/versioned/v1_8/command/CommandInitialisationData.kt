package com.gitlab.candicey.zenithcore.versioned.v1_8.command

import com.gitlab.candicey.zenithcore.util.GREY
import com.gitlab.candicey.zenithcore.util.LIGHT_PURPLE
import com.gitlab.candicey.zenithcore.util.YELLOW

/**
 * Data class for initialising [CommandManager].
 *
 * @property commandName The aliases of the command.
 * @property prefix The prefix of the command. Used to check if the message is a command.
 * @property addPrefixFunction The function that adds the prefix (not [prefix]) to the printed message.
 * @see [CommandManager]
 */
data class CommandInitialisationData(
    val commandName: List<String>,
    val prefix: String = "/",
    val addPrefixFunction: (String) -> String = { "$GREY[$LIGHT_PURPLE${commandName[0]}$GREY]$YELLOW $it" }
)
