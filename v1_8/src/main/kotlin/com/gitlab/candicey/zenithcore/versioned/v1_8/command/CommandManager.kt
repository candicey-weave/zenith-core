package com.gitlab.candicey.zenithcore.versioned.v1_8.command

import com.gitlab.candicey.zenithcore.util.*
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addChatMessage
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent
import com.gitlab.candicey.zenithcore.versioned.v1_8.helper.MessageHelper
import com.gitlab.candicey.zenithcore.versioned.v1_8.warn
import net.minecraft.event.ClickEvent
import net.minecraft.event.HoverEvent
import java.util.concurrent.Executors

private val commandsExecutor = Executors.newFixedThreadPool(50)

/**
 * The manager of commands. It will check if the message matches any of the commands and execute it.
 *
 * @property commandInitialisationData The data for initialising the command manager.
 * @see [CommandInitialisationData]
 * @see [CommandAbstract]
 */
open class CommandManager(private val commandInitialisationData: CommandInitialisationData, autoInit: Boolean = true) {
    var initialised = false
        private set

    init {
        if (autoInit) {
            init()
        }
    }

    /**
     * The list of commands.
     *
     * @see [CommandAbstract]
     */
    private val commands = mutableListOf<CommandAbstract>()

    /**
     * Check if the message matches any of the commands and execute it.
     *
     * @param raw The raw message.
     * @return If the message matches any of the commands.
     */
    private fun onMessage(raw: String): Boolean {
        if (!raw.startsWith(commandInitialisationData.prefix)) {
            return false
        }

        val message = raw.substring(commandInitialisationData.prefix.length)
        val split = message.split("\\s+".toRegex())

        if (split.isEmpty()) {
            return false
        }

        val equal = commandInitialisationData.commandName.any { split[0].equals(it, true) }
        if (!equal) {
            return false
        }

        if (split.size < 2 || split[1].equals("help", true)) {
            sendHelpMessage()
            return true
        }

        val args = split.drop(2)

        val commands = commands.filter { it.isThisCommand(split[1]) }

        if (commands.isEmpty()) {
            commandInitialisationData
                .addPrefixFunction("${RED}Command not found!")
                .toChatComponent()
                .addChatMessage()
        } else {
            commandsExecutor.submit {
                for (command in commands) {
                    command.preExecute(args, raw, emptyList())
                }
            }
        }

        return true
    }

    /**
     * Send the help message to the player. All the commands that are not hidden will be displayed.
     */
    open fun sendHelpMessage() {
        commandInitialisationData
            .addPrefixFunction("${YELLOW}${BOLD}${UNDERLINE}Available commands${YELLOW}${BOLD}:")
            .toChatComponent()
            .addChatMessage()

        for (commandAbstract in commands) {
            val prefix = commandInitialisationData.prefix
            val commandName = commandInitialisationData.commandName[0]
            val alias = commandAbstract.aliases[0]
            if (!commandAbstract.hideCommand) {
                val iChatComponent = commandInitialisationData
                    .addPrefixFunction(" $GREY$BOLD$RIGHT_ARROW$RESET ")
                    .toChatComponent()

                val command = "$prefix$commandName $alias"
                val commandComponent = "$DARK_AQUA$command".toChatComponent().apply {
                    chatStyle.apply {
                        chatClickEvent = ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, command)
                        chatHoverEvent = HoverEvent(HoverEvent.Action.SHOW_TEXT, command.toChatComponent())
                    }
                }
                iChatComponent.appendSibling(commandComponent)

                if (commandAbstract.description.isNotBlank()) {
                    iChatComponent
                        .appendSibling(" $YELLOW- ${commandAbstract.description}".toChatComponent())
                }

                iChatComponent.addChatMessage()
            }
        }
    }

    /**
     * Initialise the command manager.
     *
     * A listener will be added to [MessageHelper] to subscribe to the sending messages.
     */
    fun init(force: Boolean = false) {
        if (initialised && !force) {
            warn("Command manager already initialised! (${this::class.java.name})")
            return
        }

        MessageHelper.addC2S {
            if (onMessage(it.message)) {
                it.cancelled = true
            }
        }
    }

    /**
     * Register commands.
     *
     * @param commandAbstracts The commands to register.
     * @see [CommandAbstract]
     */
    fun registerCommand(vararg commandAbstracts: CommandAbstract) {
        commands.addAll(commandAbstracts)
    }
}