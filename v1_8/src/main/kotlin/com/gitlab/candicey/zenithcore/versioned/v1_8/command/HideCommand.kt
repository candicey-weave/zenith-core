package com.gitlab.candicey.zenithcore.versioned.v1_8.command

/**
 * Used to hide a command from the help message.
 */
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
annotation class HideCommand