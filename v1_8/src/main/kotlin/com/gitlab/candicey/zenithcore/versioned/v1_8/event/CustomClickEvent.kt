package com.gitlab.candicey.zenithcore.versioned.v1_8.event

import com.gitlab.candicey.zenithcore.util.reflectField
import net.minecraft.event.ClickEvent

/**
 * A custom click event that allows for more actions.
 *
 * @see [net.minecraft.event.ClickEvent]
 * @see [com.gitlab.candicey.zenithcore.versioned.v1_8.hook.ClickEventActionHook]
 */
object CustomClickEvent {
    private val noneField = reflectField<ClickEvent.Action>("NONE")
    val NONE: ClickEvent.Action
        get() = noneField[null] as ClickEvent.Action

    private val copyImageToClipboardField = reflectField<ClickEvent.Action>("COPY_IMAGE_TO_CLIPBOARD")
    val COPY_IMAGE_TO_CLIPBOARD: ClickEvent.Action
        get() = copyImageToClipboardField[null] as ClickEvent.Action

    private val copyTextToClipboardField = reflectField<ClickEvent.Action>("COPY_TEXT_TO_CLIPBOARD")
    val COPY_TEXT_TO_CLIPBOARD: ClickEvent.Action
        get() = copyTextToClipboardField.get(null) as ClickEvent.Action
}