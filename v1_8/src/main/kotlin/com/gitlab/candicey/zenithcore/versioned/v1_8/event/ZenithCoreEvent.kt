@file:Suppress("ConvertObjectToDataObject")

package com.gitlab.candicey.zenithcore.versioned.v1_8.event

import net.minecraft.entity.Entity
import net.minecraft.world.World
import net.weavemc.api.event.CancellableEvent
import net.weavemc.api.event.Event

/**
 * Called when the player ticks.
 *
 * @see [com.gitlab.candicey.zenithcore.versioned.v1_8.event.hook.PlayerTickEventHook]
 */
sealed class PlayerTickEvent : Event() {
    object Pre : PlayerTickEvent()

    object Post : PlayerTickEvent()
}

/**
 * Called when the entity joins the world.
 *
 * @see [com.gitlab.candicey.zenithcore.versioned.v1_8.event.hook.EntityJoinWorldEventHook]
 */
class EntityJoinWorldEvent(val entity: Entity, val world: World) : CancellableEvent()

/**
 * Called after the game processes the key press.
 *
 * @see [com.gitlab.candicey.zenithcore.versioned.v1_8.event.hook.LateKeyboardEventHook]
 */
class LateKeyboardEvent : Event()