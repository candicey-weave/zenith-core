package com.gitlab.candicey.zenithcore.versioned.v1_8.event.hook

import com.gitlab.candicey.zenithcore.util.weave.callEvent
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.EntityJoinWorldEvent
import net.weavemc.api.Hook
import net.weavemc.internals.asm
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.LabelNode
import org.objectweb.asm.tree.MethodInsnNode
import org.objectweb.asm.tree.VarInsnNode

/**
 * A [com.gitlab.candicey.zenithcore.versioned.v1_8.event.EntityJoinWorldEvent] is posted when the [net.minecraft.entity.Entity] joins the world.
 *
 * @see [net.minecraft.world.World.spawnEntityInWorld]
 */
internal object EntityJoinWorldEventHook : Hook("net/minecraft/world/World") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val methodNode = node.methods.named("spawnEntityInWorld")

        val instruction = methodNode.instructions.find { insnNode ->
            insnNode is VarInsnNode && insnNode.opcode == Opcodes.ALOAD && insnNode.`var` == 0
                && insnNode.next.let { nextInsnNode ->
                nextInsnNode != null && nextInsnNode is MethodInsnNode && nextInsnNode.opcode == Opcodes.INVOKEVIRTUAL && nextInsnNode.owner == "net/minecraft/world/World" && nextInsnNode.name == "updateAllPlayersSleepingFlag"
                    && nextInsnNode.next.let { nextNextInsnNode ->
                    nextNextInsnNode != null && nextNextInsnNode is LabelNode
                }
            }
        }

        if (instruction != null) {
            methodNode.instructions.insert(instruction.next.next, asm {
                new(internalNameOf<EntityJoinWorldEvent>())
                dup
                dup
                aload(1)
                aload(0)
                invokespecial(
                    internalNameOf<EntityJoinWorldEvent>(),
                    "<init>",
                    "(Lnet/minecraft/entity/Entity;Lnet/minecraft/world/World;)V"
                )
                callEvent()

                val end = LabelNode()

                invokevirtual(internalNameOf<EntityJoinWorldEvent>(), "isCancelled", "()Z")
                ifeq(end)

                iload(4)
                ifne(end)

                iconst_0
                ireturn

                +end
            })

            cfg.computeFrames()
        }
    }
}
