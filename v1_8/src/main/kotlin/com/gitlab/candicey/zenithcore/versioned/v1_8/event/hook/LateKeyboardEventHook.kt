package com.gitlab.candicey.zenithcore.versioned.v1_8.event.hook

import com.gitlab.candicey.zenithcore.util.weave.callEvent
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.LateKeyboardEvent
import net.weavemc.api.Hook
import net.weavemc.internals.asm
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.VarInsnNode

/**
 * A [com.gitlab.candicey.zenithcore.versioned.v1_8.event.LateKeyboardEvent] is posted after the game processes the key press.
 *
 * @see [net.minecraft.client.Minecraft.runTick]
 */
internal object LateKeyboardEventHook : Hook("net/minecraft/client/Minecraft") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val insnList = node.methods.named("runTick").instructions

        insnList.find {
            it.opcode == Opcodes.ILOAD && (it as VarInsnNode).`var` == 1 && it.next.let { next ->
                next.opcode == Opcodes.ICONST_1 && next.next.let { nextNext ->
                    nextNext.opcode == Opcodes.IF_ICMPNE
                }
            }
        }?.let {
            insnList.insert(it, asm {
                new(internalNameOf<LateKeyboardEvent>())
                dup
                invokespecial(
                    internalNameOf<LateKeyboardEvent>(),
                    "<init>",
                    "()V"
                )
                callEvent()
            })
        }
    }
}