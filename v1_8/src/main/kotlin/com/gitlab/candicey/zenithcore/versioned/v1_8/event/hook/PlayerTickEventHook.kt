package com.gitlab.candicey.zenithcore.versioned.v1_8.event.hook

import com.gitlab.candicey.zenithcore.extension.insertBeforeReturn
import com.gitlab.candicey.zenithcore.util.weave.callEvent
import com.gitlab.candicey.zenithcore.util.weave.getSingleton
import com.gitlab.candicey.zenithcore.util.weave.named
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.PlayerTickEvent
import net.weavemc.api.Hook
import net.weavemc.internals.asm
import org.objectweb.asm.tree.ClassNode

/**
 * A [com.gitlab.candicey.zenithcore.versioned.v1_8.event.PlayerTickEvent] is posted when the player ticks.
 *
 * @see [net.minecraft.entity.player.EntityPlayer.onUpdate]
 */
internal object PlayerTickEventHook : Hook("net/minecraft/entity/player/EntityPlayer") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val onUpdate = node.methods.named("onUpdate")
        onUpdate.instructions.insert(asm {
            getSingleton<PlayerTickEvent.Pre>()
            callEvent()
        })

        onUpdate.instructions.insertBeforeReturn(asm {
            getSingleton<PlayerTickEvent.Post>()
            callEvent()
        })
    }
}
