package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.event.ClickEvent
import net.minecraft.util.ChatStyle

var ChatStyle.zenithClickEvent: ClickEvent? by ShadowField()