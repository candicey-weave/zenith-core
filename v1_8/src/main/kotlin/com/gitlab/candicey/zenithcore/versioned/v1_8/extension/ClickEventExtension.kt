package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.event.ClickEvent

var ClickEvent.zenithRunnable: Runnable? by ShadowField()