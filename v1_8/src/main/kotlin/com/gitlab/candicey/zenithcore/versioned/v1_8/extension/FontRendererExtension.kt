package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import com.gitlab.candicey.zenithcore.util.reflectMethod
import net.minecraft.client.gui.FontRenderer

private val trimStringNewLineMethod = reflectMethod<FontRenderer>("trimStringNewline", String::class.java)
fun FontRenderer.trimStringNewLine(text: String): String {
    return trimStringNewLineMethod.invoke(this, text) as String
}

private val sizeStringToWidthMethod = reflectMethod<FontRenderer>("sizeStringToWidth", String::class.java, Int::class.javaPrimitiveType!!)
fun FontRenderer.sizeStringToWidth(text: String, width: Int): Int {
    return sizeStringToWidthMethod.invoke(this, text, width) as Int
}

fun FontRenderer.drawCenteredString(text: String, x: Int, y: Int, color: Int, shadow: Boolean = true) {
    drawString(text, (x - getStringWidth(text) / 2).toFloat(), y.toFloat(), color, shadow)
}

fun FontRenderer.drawCenteredString(text: String, x: Float, y: Float, color: Int, shadow: Boolean = true) {
    drawString(text, x - getStringWidth(text) / 2F, y, color, shadow)
}