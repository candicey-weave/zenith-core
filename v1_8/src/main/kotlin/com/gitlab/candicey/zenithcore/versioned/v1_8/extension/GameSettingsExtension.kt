package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import net.minecraft.client.settings.GameSettings
import net.minecraft.client.settings.KeyBinding

fun GameSettings.addKeyBindings(vararg bindings: KeyBinding) {
    keyBindings += bindings
}