package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.client.gui.GuiButton

var GuiButton.width: Int by ShadowField()

var GuiButton.height: Int by ShadowField()