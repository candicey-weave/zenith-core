package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.client.gui.GuiChat
import net.minecraft.client.gui.GuiTextField

var GuiChat.inputField: GuiTextField by ShadowField()