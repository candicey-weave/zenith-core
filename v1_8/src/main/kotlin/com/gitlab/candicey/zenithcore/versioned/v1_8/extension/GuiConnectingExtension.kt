package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.client.gui.GuiScreen
import net.minecraft.client.multiplayer.GuiConnecting
import net.minecraft.network.NetworkManager

var GuiConnecting.cancel: Boolean by ShadowField()

var GuiConnecting.networkManager: NetworkManager? by ShadowField()

val GuiConnecting.previousGuiScreen: GuiScreen? by ShadowField()