package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import com.gitlab.candicey.zenithcore.versioned.v1_8.fontRenderer
import net.minecraft.client.gui.FontRenderer
import net.minecraft.client.gui.Gui

fun Gui.drawString(text: String, x: Int, y: Int, color: Int, shadow: Boolean = true, fontRendererObj: FontRenderer = fontRenderer) {
    if (shadow) {
        fontRendererObj.drawStringWithShadow(text, x.toFloat(), y.toFloat(), color)
    } else {
        fontRendererObj.drawString(text, x, y, color)
    }
}

fun Gui.drawString(text: String, x: Float, y: Float, color: Int, shadow: Boolean = true, fontRendererObj: FontRenderer = fontRenderer) {
    if (shadow) {
        fontRendererObj.drawStringWithShadow(text, x, y, color)
    } else {
        fontRendererObj.drawString(text, x.toInt(), y.toInt(), color)
    }
}

fun Gui.drawStringRightAligned(text: String, x: Int, y: Int, color: Int, shadow: Boolean = true, fontRendererObj: FontRenderer = fontRenderer) {
    if (shadow) {
        fontRendererObj.drawStringWithShadow(text, (x - fontRendererObj.getStringWidth(text)).toFloat(), y.toFloat(), color)
    } else {
        fontRendererObj.drawString(text, x - fontRendererObj.getStringWidth(text), y, color)
    }
}

fun Gui.drawStringRightAligned(text: String, x: Float, y: Float, color: Int, shadow: Boolean = true, fontRendererObj: FontRenderer = fontRenderer) {
    if (shadow) {
        fontRendererObj.drawStringWithShadow(text, (x - fontRendererObj.getStringWidth(text)), y, color)
    } else {
        fontRendererObj.drawString(text, x - fontRendererObj.getStringWidth(text), y, color, false)
    }
}

fun Gui.drawCenteredString(text: String, x: Int, y: Int, color: Int, shadow: Boolean = true, fontRendererObj: FontRenderer = fontRenderer) {
    if (shadow) {
        fontRendererObj.drawStringWithShadow(text, (x - fontRendererObj.getStringWidth(text) / 2).toFloat(), y.toFloat(), color)
    } else {
        fontRendererObj.drawString(text, x - fontRendererObj.getStringWidth(text) / 2, y, color)
    }
}

fun Gui.drawCenteredString(text: String, x: Float, y: Float, color: Int, shadow: Boolean = true, fontRendererObj: FontRenderer = fontRenderer) {
    if (shadow) {
        fontRendererObj.drawStringWithShadow(text, (x - fontRendererObj.getStringWidth(text) / 2), y, color)
    } else {
        fontRendererObj.drawString(text, x - fontRendererObj.getStringWidth(text) / 2, y, color, false)
    }
}
