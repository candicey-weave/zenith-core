package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import com.gitlab.candicey.zenithcore.util.reflectMethod
import net.minecraft.client.gui.ChatLine
import net.minecraft.client.gui.GuiNewChat
import net.minecraft.util.IChatComponent

val GuiNewChat.chatLines: MutableList<ChatLine> by ShadowField()

val GuiNewChat.drawnChatLines: MutableList<ChatLine> by ShadowField()

private val setChatLineMethod = reflectMethod<GuiNewChat>("setChatLine", IChatComponent::class.java, Int::class.java, Int::class.java, Boolean::class.java)
fun GuiNewChat.setChatLine(chatComponent: IChatComponent, chatLineId: Int, updateCounter: Int, displayOnly: Boolean) =
    setChatLineMethod.invoke(this, chatComponent, chatLineId, updateCounter, displayOnly)