package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.gui.GuiScreen

val GuiScreen.buttonList: MutableList<GuiButton> by ShadowField()

fun GuiScreen.addButton(vararg guiButton: GuiButton) {
    buttonList.addAll(guiButton)
}