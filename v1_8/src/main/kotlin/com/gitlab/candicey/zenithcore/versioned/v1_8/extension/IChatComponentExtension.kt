package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import net.minecraft.util.ChatComponentText
import net.minecraft.util.IChatComponent

var IChatComponent.ignoreMessageHook: Boolean?
    get() = (this as? ChatComponentText)?.ignoreMessageHook
    set(value) {
        if (this is ChatComponentText && value != null) {
            ignoreMessageHook = value
        }
    }

fun IChatComponent.addChatMessage(ignoreMessageHook: Boolean = true) =
    runCatching { mc.thePlayer.addChatMessage(apply { this.ignoreMessageHook = ignoreMessageHook }) }

fun IChatComponent.sendChatMessage() =
    runCatching { mc.thePlayer.sendChatMessage(unformattedText) }

fun IChatComponent.equalsOther(other: IChatComponent): Boolean {
    if (siblings.size != other.siblings.size) {
        return false
    }

    if (siblings.isEmpty()) {
        if (unformattedText != other.unformattedText) {
            return false
        }

        if (chatStyle.chatHoverEvent.action != other.chatStyle.chatHoverEvent.action) {
            return false
        }

        if (chatStyle.chatHoverEvent.value.unformattedText != other.chatStyle.chatHoverEvent.value.unformattedText) {
            return false
        }

        if (chatStyle.chatClickEvent.action != other.chatStyle.chatClickEvent.action) {
            return false
        }

        if (chatStyle.chatClickEvent.value != other.chatStyle.chatClickEvent.value) {
            return false
        }

        return true
    } else {
        siblings.forEachIndexed { index, iChatComponent ->
            if (!iChatComponent.equals(other.siblings[index])) {
                return false
            }
        }

        return true
    }
}