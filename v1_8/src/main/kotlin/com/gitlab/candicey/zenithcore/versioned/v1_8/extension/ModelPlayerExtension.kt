package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.client.model.ModelPlayer
import net.minecraft.client.model.ModelRenderer

var ModelPlayer.bipedDeadmau5Head: ModelRenderer by ShadowField()