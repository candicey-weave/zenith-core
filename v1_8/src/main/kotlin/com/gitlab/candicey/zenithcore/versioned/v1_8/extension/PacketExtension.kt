package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import com.gitlab.candicey.zenithcore.util.reflectField
import net.minecraft.network.handshake.client.C00Handshake
import net.minecraft.network.play.server.S3APacketTabComplete

private val protocolVersionField = reflectField<C00Handshake>("protocolVersion")
fun C00Handshake.setProtocolVersion(value: Int) = protocolVersionField.set(this, value)

var C00Handshake.ip: String by ShadowField()

var C00Handshake.port: Int by ShadowField()

var S3APacketTabComplete.matches: Array<String>? by ShadowField()