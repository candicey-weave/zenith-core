package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.client.renderer.entity.RenderManager
import net.minecraft.client.renderer.entity.RenderPlayer

var RenderManager.skinMap: MutableMap<String, RenderPlayer> by ShadowField()
