package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import com.gitlab.candicey.zenithcore.util.ShadowField
import net.minecraft.client.renderer.entity.RendererLivingEntity
import net.minecraft.client.renderer.entity.layers.LayerRenderer
import net.minecraft.entity.EntityLivingBase

var RendererLivingEntity<out EntityLivingBase>.layerRenderers: MutableList<LayerRenderer<*>> by ShadowField()