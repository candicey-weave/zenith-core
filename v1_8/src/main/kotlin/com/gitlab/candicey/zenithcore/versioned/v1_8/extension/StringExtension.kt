package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import net.minecraft.util.ChatComponentText
import net.minecraft.util.IChatComponent

/**
 * Converts this string to a [IChatComponent] object.
 */
fun String.toChatComponent(): IChatComponent = ChatComponentText(this)