package com.gitlab.candicey.zenithcore.versioned.v1_8.extension

import com.gitlab.candicey.zenithcore.versioned.v1_8.font.TTFFontRenderer

fun TTFFontRenderer.drawString(text: String, x: Float, y: Float, color: Int, shadow: Boolean = true) {
    if (shadow) {
        drawStringWithShadow(text, x, y, color)
    } else {
        drawString(text, x, y, color)
    }
}

fun TTFFontRenderer.drawString(text: String, x: Int, y: Int, color: Int, shadow: Boolean = true) {
    if (shadow) {
        drawStringWithShadow(text, x.toFloat(), y.toFloat(), color)
    } else {
        drawString(text, x, y, color)
    }
}

fun TTFFontRenderer.drawStringRightAligned(text: String, x: Float, y: Float, color: Int, shadow: Boolean = true) {
    if (shadow) {
        drawStringWithShadow(text, x - getWidth(text), y, color)
    } else {
        drawString(text, x - getHeight(text), y, color)
    }
}

fun TTFFontRenderer.drawStringRightAligned(text: String, x: Int, y: Int, color: Int, shadow: Boolean = true) {
    if (shadow) {
        drawStringWithShadow(text, x - getWidth(text), y.toFloat(), color)
    } else {
        drawString(text, x - getHeight(text), y.toFloat(), color)
    }
}

fun TTFFontRenderer.drawCenteredString(text: String, x: Float, y: Float, color: Int, shadow: Boolean = true) {
    if (shadow) {
        drawStringWithShadow(text, x - getWidth(text) / 2, y, color)
    } else {
        drawString(text, x - getWidth(text) / 2, y, color)
    }
}

fun TTFFontRenderer.drawCenteredString(text: String, x: Int, y: Int, color: Int, shadow: Boolean = true) {
    if (shadow) {
        drawStringWithShadow(text, x - getWidth(text) / 2, y.toFloat(), color)
    } else {
        drawString(text, x - getWidth(text) / 2, y.toFloat(), color)
    }
}