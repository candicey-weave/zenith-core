package com.gitlab.candicey.zenithcore.versioned.v1_8.font

import com.gitlab.candicey.zenithcore.versioned.v1_8.info
import java.awt.Font
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

object FontManager {
    private val fonts = mutableMapOf<String, TTFFontRenderer>()

    private val registerFonts = mutableListOf<Triple<String, Int, Int>>()

    private val executor = Executors.newFixedThreadPool(100)

    /**
     * Initialises all [registerFonts] asynchronously.
     */
    internal fun init() {
        registerFonts
            .associateBy { "${it.first} ${it.second} ${it.third}" }
            .forEach {
                executor.submit {
                    Thread.currentThread().name = "[ZenithCore] FontManager - ${it.key}"

                    if (!fonts.containsKey(it.key)) {
                        fonts[it.key] =
                            TTFFontRenderer(
                                Font(
                                    it.value.first,
                                    it.value.third,
                                    it.value.second
                                )
                            )
                        info("FontManager - Registered font: ${it.key}")
                    }
                }
            }
    }

    /**
     * Waits until all fonts are registered.
     *
     * @see [init]
     */
    internal fun waitUntilReady() {
        executor.shutdown()
        executor.awaitTermination(60, TimeUnit.SECONDS)
    }

    /**
     * Registers a font for faster access later. This method must be called before [org.lwjgl.opengl.GL11] is initialised. It is recommended to call this method in [net.weavemc.loader.api.ModInitializer.preInit].
     *
     * Example: `FontManager.registerFont("Verdana", 24, Font.BOLD)`
     */
    fun registerFont(fontName: String, fontSize: Int, fontStyle: Int = Font.PLAIN) {
        if (!fonts.containsKey("$fontName $fontSize $fontStyle")) {
            registerFonts.add(Triple(fontName, fontSize, fontStyle))
        }
    }

    /**
     * Gets a font from the [fonts] map. If the font is not registered, it will be registered.
     *
     * Example: `val fontRenderer = FontManager.getFont("Verdana", 24, Font.BOLD)`
     */
    fun getFont(fontName: String, fontSize: Int, fontStyle: Int = Font.PLAIN): TTFFontRenderer {
        val name = "$fontName $fontSize $fontStyle"
        return fonts[name] ?: run {
            fonts[name] = TTFFontRenderer(
                Font(
                    fontName,
                    fontStyle,
                    fontSize
                )
            )
            fonts[name]!!
        }
    }
}