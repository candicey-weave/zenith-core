package com.gitlab.candicey.zenithcore.versioned.v1_8.gui

import com.gitlab.candicey.zenithcore.versioned.v1_8.child.GuiScreenChild
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.drawCenteredString
import net.minecraft.client.renderer.GlStateManager
import org.lwjgl.input.Keyboard

open class ConfigGui(private val screenName: String, configs: List<ConfigOption>) : GuiScreenChild() {
    val configs = configs.toMutableList()

    private val margin = 10

    override fun initGui() {
        Keyboard.enableRepeatEvents(true)

        configs.forEach(ConfigOption::initGui)
    }

    override fun onGuiClosed() {
        Keyboard.enableRepeatEvents(false)

        configs.forEach(ConfigOption::onGuiClosed)
    }

    override fun drawScreen(mouseX: Int, mouseY: Int, partialTicks: Float) {
        drawRect(0, 0, width, height, 0xCC000000.toInt())

        GlStateManager.pushMatrix()
        GlStateManager.scale(1.5f, 1.5f, 1.5f)
        fontRendererObj.drawCenteredString(screenName, width / 2 / 1.5f, 25 / 1.5f, 0xFFFFFF)
        GlStateManager.popMatrix()

        var startY = 65

        for (config in configs) {
            config.apply {
                this.startY = startY
                this.mouseX = mouseX
                this.mouseY = mouseY
            }

            config.onRender()
            startY += config.height + margin
        }
    }

    override fun mouseClicked(mouseX: Int, mouseY: Int, mouseButton: Int) {
        for (config in configs) {
            config.apply {
                this.mouseX = mouseX
                this.mouseY = mouseY
            }

            config.onClick(mouseButton)
        }
    }

    override fun keyTyped(char: Char, keyCode: Int) {
        super.keyTyped(char, keyCode)

        for (config in configs) {
            config.onKeyTyped(char, keyCode)
        }
    }
}