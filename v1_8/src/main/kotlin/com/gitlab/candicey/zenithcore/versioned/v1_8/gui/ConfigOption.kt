package com.gitlab.candicey.zenithcore.versioned.v1_8.gui

import com.gitlab.candicey.zenithcore.versioned.v1_8.fontRenderer
import com.gitlab.candicey.zenithcore.versioned.v1_8.gameWidth

open class ConfigOption {
    var mouseX = -1
    var mouseY = -1

    var startY = -1

    var marginLeft = 20
    var marginRight = 20
    open val width: Int
        get() = gameWidth - marginLeft - marginRight

    open val height: Int = 25

    open fun initGui() {
    }

    open fun onGuiClosed() {
    }

    open fun onRender() {
    }

    open fun onClick(buttonCode: Int) {
    }

    open fun onKeyTyped(char: Char, keyCode: Int) {
    }

    fun isMouseOver(): Boolean = mouseX in marginLeft..marginLeft + width && mouseY in startY..startY + height

    protected fun renderText(text: String) {
        val startY = startY + (height - fontRenderer.FONT_HEIGHT) / 2 - 2
        fontRenderer.drawString(text, marginLeft, startY, 0xFFFFFFFF.toInt())
    }
}