package com.gitlab.candicey.zenithcore.versioned.v1_8.gui

import com.gitlab.candicey.zenithcore.extension.findIsInstance
import com.gitlab.candicey.zenithcore.util.reflectField
import com.gitlab.candicey.zenithcore.util.reflectMethod
import com.gitlab.candicey.zenithcore.versioned.v1_8.gui.ConfigProperties.*
import com.gitlab.candicey.zenithcore.versioned.v1_8.gui.type.ButtonOption
import com.gitlab.candicey.zenithcore.versioned.v1_8.gui.type.TextOption

object ConfigOptionsGenerator {
    fun generateConfigOptions(configObject: Any): List<ConfigOption> =
        configObject
            .javaClass
            .declaredFields
            .map {
                it.isAccessible = true
                try {
                    getConfigOptionType(it.name, it.get(configObject), it.annotations.toList())
                } catch (e: IllegalArgumentException) {
                    throw IllegalArgumentException("${e.message} (Config: ${configObject.javaClass.name}, Field: ${it.name})")
                }
            }

    private fun getConfigOptionType(name: String, value: Any?, annotations: List<Annotation>): ConfigOption {
        val shadowFieldAnnotation = annotations.findIsInstance<Shadow.Field>()
        val shadowMethodAnnotation = annotations.findIsInstance<Shadow.Method>()
        val displayTextAnnotation = annotations.findIsInstance<DisplayText>()
        val typeAnnotation = annotations.findIsInstance<Type>()

        // Button
        val clickHandlerAnnotation = annotations.findIsInstance<Button.ClickHandler>()
        val buttonTextAnnotation = annotations.findIsInstance<Button.ButtonText>()
        val booleanMethodReflectionMatcherAnnotation = annotations.findIsInstance<Button.Boolean.ReflectionMatcher.Method>()

        // Text
        val maxLengthAnnotation = annotations.findIsInstance<Text.MaxLength>()
        val textFieldReflectionMatcherAnnotation = annotations.findIsInstance<Text.ReflectionMatcher.Field>()
        val textMethodReflectionMatcherAnnotation = annotations.findIsInstance<Text.ReflectionMatcher.Method>()

        // Number
        val minAnnotation = annotations.findIsInstance<Text.Number.Min>()
        val maxAnnotation = annotations.findIsInstance<Text.Number.Max>()

        val displayText = displayTextAnnotation?.text ?: name
        val buttonDisplayText = buttonTextAnnotation?.text ?: name

        typeAnnotation?.let { type ->
            return when (type.configOptionType) {
                // Button
                ButtonOption.TYPE -> createButton(displayText, buttonDisplayText, clickHandlerAnnotation)
                ButtonOption.Boolean.TYPE -> createButtonBoolean(displayText, buttonDisplayText, shadowFieldAnnotation, shadowMethodAnnotation, clickHandlerAnnotation, booleanMethodReflectionMatcherAnnotation)

                // Text
                TextOption.TYPE -> createText(displayText, value, shadowFieldAnnotation, shadowMethodAnnotation, maxLengthAnnotation, textFieldReflectionMatcherAnnotation, textMethodReflectionMatcherAnnotation)
                TextOption.Integer.TYPE -> createTextInteger(displayText, value.toString().toIntOrNull() ?: throw IllegalArgumentException("Value cannot be null!"), shadowFieldAnnotation, shadowMethodAnnotation, maxLengthAnnotation, minAnnotation, maxAnnotation, textFieldReflectionMatcherAnnotation, textMethodReflectionMatcherAnnotation)
                TextOption.Float.TYPE -> createTextFloat(displayText, value.toString().toFloatOrNull() ?: throw IllegalArgumentException("Value cannot be null!"), shadowFieldAnnotation, shadowMethodAnnotation, maxLengthAnnotation, minAnnotation, maxAnnotation, textFieldReflectionMatcherAnnotation, textMethodReflectionMatcherAnnotation)

                else -> throw IllegalArgumentException("Invalid config option type!")
            }
        }

        return when (value) {
            is Boolean -> createButtonBoolean(displayText, value, shadowFieldAnnotation, shadowMethodAnnotation, clickHandlerAnnotation, booleanMethodReflectionMatcherAnnotation)

            is Int -> createTextInteger(displayText, value, shadowFieldAnnotation, shadowMethodAnnotation, maxLengthAnnotation, minAnnotation, maxAnnotation, textFieldReflectionMatcherAnnotation, textMethodReflectionMatcherAnnotation)

            is Float, is Double -> createTextFloat(displayText, value.toString().toFloat(), shadowFieldAnnotation, shadowMethodAnnotation, maxLengthAnnotation, minAnnotation, maxAnnotation, textFieldReflectionMatcherAnnotation, textMethodReflectionMatcherAnnotation)

            is String -> createText(displayText, value, shadowFieldAnnotation, shadowMethodAnnotation, maxLengthAnnotation, textFieldReflectionMatcherAnnotation, textMethodReflectionMatcherAnnotation)

            else -> throw IllegalArgumentException("Invalid config option type!")
        }
    }

    private fun createButton(
        displayText: String,
        buttonDisplayText: String,
        clickHandlerAnnotation: Button.ClickHandler?
    ) = ButtonOption(
        displayText,
        buttonDisplayText
    ).apply {
        clickHandlerAnnotation?.run {
            onButtonClick = { button ->
                reflectMethod(ownerClass, methodName, ButtonOption::class.java)
                    .invoke(null, button)
            }
        }
    }

    private fun createButtonBoolean(
        displayText: String,
        value: Any?,
        shadowFieldAnnotation: Shadow.Field?,
        shadowMethodAnnotation: Shadow.Method?,
        clickHandlerAnnotation: Button.ClickHandler?,
        booleanMethodReflectionMatcherAnnotation: Button.Boolean.ReflectionMatcher.Method?
    ) = ButtonOption.Boolean(
        displayText,
        value as? Boolean ?: throw IllegalArgumentException("Config value is not a boolean!")
    ).apply {
        onGuiClosedBoolean = { buttonBooleanOption ->
            shadowFieldAnnotation?.run {
                if (callWhen == Shadow.CallWhen.GUI_CLOSED || callWhen == Shadow.CallWhen.BOTH) {
                    reflectField(ownerClass, fieldName)
                        .set(null, buttonBooleanOption.value)
                }
            }
            shadowMethodAnnotation?.run {
                if (callWhen == Shadow.CallWhen.GUI_CLOSED || callWhen == Shadow.CallWhen.BOTH) {
                    reflectMethod(ownerClass, methodName, Boolean::class.java)
                        .invoke(null, buttonBooleanOption.value)
                }
            }
        }

        onValueChange = { newValue ->
            shadowFieldAnnotation?.run {
                if (callWhen == Shadow.CallWhen.VALUE_CHANGE || callWhen == Shadow.CallWhen.BOTH) {
                    reflectField(ownerClass, fieldName)
                        .set(null, newValue)
                }
            }
            shadowMethodAnnotation?.run {
                if (callWhen == Shadow.CallWhen.VALUE_CHANGE || callWhen == Shadow.CallWhen.BOTH) {
                    reflectMethod(ownerClass, methodName, Boolean::class.java)
                        .invoke(null, newValue)
                }
            }
        }

        clickHandlerAnnotation?.run {
            onButtonClick = { button ->
                reflectMethod(ownerClass, methodName, ButtonOption::class.java)
                    .invoke(null, button)
            }
        }

        booleanMethodReflectionMatcherAnnotation?.run {
            allowValueChange = { newValue ->
                reflectMethod(ownerClass, methodName, Boolean::class.java)
                    .invoke(null, newValue) as Boolean
            }
        }
    }

    private fun createText(
        displayText: String,
        value: Any?,
        shadowFieldAnnotation: Shadow.Field?,
        shadowMethodAnnotation: Shadow.Method?,
        maxLengthAnnotation: Text.MaxLength?,
        textFieldReflectionMatcherAnnotation: Text.ReflectionMatcher.Field?,
        textMethodReflectionMatcherAnnotation: Text.ReflectionMatcher.Method?
    ) = TextOption(
        displayText,
        value.toString()
    ).apply {
        onGuiClosed = { textOption ->
            shadowFieldAnnotation?.run {
                if (callWhen == Shadow.CallWhen.GUI_CLOSED || callWhen == Shadow.CallWhen.BOTH) {
                    reflectField(ownerClass, fieldName)
                        .set(null, textOption.textField.text)
                }
            }
            shadowMethodAnnotation?.run {
                if (callWhen == Shadow.CallWhen.GUI_CLOSED || callWhen == Shadow.CallWhen.BOTH) {
                    reflectMethod(ownerClass, methodName, String::class.java)
                        .invoke(null, textOption.textField.text)
                }
            }
        }

        onTextChange = { _, newValue ->
            shadowFieldAnnotation?.run {
                if (callWhen == Shadow.CallWhen.VALUE_CHANGE || callWhen == Shadow.CallWhen.BOTH) {
                    reflectField(ownerClass, fieldName)
                        .set(null, newValue)
                }
            }
            shadowMethodAnnotation?.run {
                if (callWhen == Shadow.CallWhen.VALUE_CHANGE || callWhen == Shadow.CallWhen.BOTH) {
                    reflectMethod(ownerClass, methodName, String::class.java)
                        .invoke(null, newValue)
                }
            }
        }

        val maxLength = { newValue: String ->
            maxLengthAnnotation?.run {
                newValue.length <= maxLength
            } ?: true
        }
        val fieldMatch = { newValue: String ->
            textFieldReflectionMatcherAnnotation?.run {
                reflectField(ownerClass, fieldName)
                    .get(null) as? Regex
            }?.matches(newValue) ?: true
        }
        val methodMatch = { oldValue: String, newValue: String ->
            textMethodReflectionMatcherAnnotation?.run {
                reflectMethod(ownerClass, methodName, String::class.java, String::class.java)
                    .invoke(null, oldValue, newValue) as? Boolean
            } ?: true
        }

        allowTextChange = { oldValue, newValue ->
            maxLength(newValue) && fieldMatch(newValue) && methodMatch(oldValue, newValue)
        }
    }

    private fun createTextInteger(
        displayText: String,
        value: Int,
        shadowFieldAnnotation: Shadow.Field?,
        shadowMethodAnnotation: Shadow.Method?,
        maxLengthAnnotation: Text.MaxLength?,
        minAnnotation: Text.Number.Min?,
        maxAnnotation: Text.Number.Max?,
        textFieldReflectionMatcherAnnotation: Text.ReflectionMatcher.Field?,
        textMethodReflectionMatcherAnnotation: Text.ReflectionMatcher.Method?
    ) = TextOption.Integer(
        displayText,
        value,
    ).apply {
        onGuiClosed = { textIntegerOption ->
            shadowFieldAnnotation?.run {
                if (callWhen == Shadow.CallWhen.GUI_CLOSED || callWhen == Shadow.CallWhen.BOTH) {
                    reflectField(ownerClass, fieldName)
                        .set(null, textIntegerOption.textField.text.toIntOrNull() ?: value)
                }
            }
            shadowMethodAnnotation?.run {
                if (callWhen == Shadow.CallWhen.GUI_CLOSED || callWhen == Shadow.CallWhen.BOTH) {
                    reflectMethod(ownerClass, methodName, Int::class.java)
                        .invoke(null, textIntegerOption.textField.text.toIntOrNull() ?: value)
                }
            }
        }

        onValueChange = { _, newValue ->
            shadowFieldAnnotation?.run {
                if (callWhen == Shadow.CallWhen.VALUE_CHANGE || callWhen == Shadow.CallWhen.BOTH) {
                    newValue?.let {
                        reflectField(ownerClass, fieldName)
                            .set(null, it)
                    }
                }
            }
            shadowMethodAnnotation?.run {
                if (callWhen == Shadow.CallWhen.VALUE_CHANGE || callWhen == Shadow.CallWhen.BOTH) {
                    newValue?.let {
                        reflectMethod(ownerClass, methodName, Int::class.java)
                            .invoke(null, it)
                    }
                }
            }
        }

        val maxLength = { newValue: Int ->
            maxLengthAnnotation?.run {
                newValue.toString().length <= maxLength
            } ?: true
        }
        val fieldMatch = { newValue: Int ->
            textFieldReflectionMatcherAnnotation?.run {
                reflectField(ownerClass, fieldName)
                    .get(null) as? Regex
            }?.matches(newValue.toString()) ?: true
        }
        val methodMatch = { oldValue: Int, newValue: Int ->
            textMethodReflectionMatcherAnnotation?.run {
                reflectMethod(ownerClass, methodName, Int::class.java, Int::class.java)
                    .invoke(null, oldValue, newValue) as? Boolean
            } ?: true
        }
        val min = { newValue: Int ->
            minAnnotation?.run {
                newValue >= min
            } ?: true
        }
        val max = { newValue: Int ->
            maxAnnotation?.run {
                newValue <= max
            } ?: true
        }

        allowValueChange = { oldValue, newValue ->
            maxLength(newValue) && min(newValue) && max(newValue) && fieldMatch(newValue) && methodMatch(oldValue, newValue)
        }
    }

    private fun createTextFloat(
        displayText: String,
        value: Float,
        shadowFieldAnnotation: Shadow.Field?,
        shadowMethodAnnotation: Shadow.Method?,
        maxLengthAnnotation: Text.MaxLength?,
        minAnnotation: Text.Number.Min?,
        maxAnnotation: Text.Number.Max?,
        textFieldReflectionMatcherAnnotation: Text.ReflectionMatcher.Field?,
        textMethodReflectionMatcherAnnotation: Text.ReflectionMatcher.Method?
    ) = TextOption.Float(
        displayText,
        value,
    ).apply {
        onGuiClosed = { textFloatOption ->
            shadowFieldAnnotation?.run {
                if (callWhen == Shadow.CallWhen.GUI_CLOSED || callWhen == Shadow.CallWhen.BOTH) {
                    reflectField(ownerClass, fieldName)
                        .set(null, textFloatOption.textField.text.toFloatOrNull() ?: value)
                }
            }
            shadowMethodAnnotation?.run {
                if (callWhen == Shadow.CallWhen.GUI_CLOSED || callWhen == Shadow.CallWhen.BOTH) {
                    reflectMethod(ownerClass, methodName, Float::class.java)
                        .invoke(null, textFloatOption.textField.text.toFloatOrNull() ?: value)
                }
            }
        }

        onValueChange = { _, newValue ->
            shadowFieldAnnotation?.run {
                if (callWhen == Shadow.CallWhen.VALUE_CHANGE || callWhen == Shadow.CallWhen.BOTH) {
                    newValue?.let {
                        reflectField(ownerClass, fieldName)
                            .set(null, it)
                    }
                }
            }
            shadowMethodAnnotation?.run {
                if (callWhen == Shadow.CallWhen.VALUE_CHANGE || callWhen == Shadow.CallWhen.BOTH) {
                    newValue?.let {
                        reflectMethod(ownerClass, methodName, Float::class.java)
                            .invoke(null, it)
                    }
                }
            }
        }

        val maxLength = { newValue: Float ->
            maxLengthAnnotation?.run {
                newValue.toString().length <= maxLength
            } ?: true
        }
        val fieldMatch = { newValue: Float ->
            textFieldReflectionMatcherAnnotation?.run {
                reflectField(ownerClass, fieldName)
                    .get(null) as? Regex
            }?.matches(newValue.toString()) ?: true
        }
        val methodMatch = { oldValue: Float, newValue: Float ->
            textMethodReflectionMatcherAnnotation?.run {
                reflectMethod(ownerClass, methodName, Float::class.java, Float::class.java)
                    .invoke(null, oldValue, newValue) as? Boolean
            } ?: true
        }
        val min = { newValue: Float ->
            minAnnotation?.run {
                newValue >= min
            } ?: true
        }
        val max = { newValue: Float ->
            maxAnnotation?.run {
                newValue <= max
            } ?: true
        }

        allowValueChange = { oldValue, newValue ->
            maxLength(newValue) && min(newValue) && max(newValue) && fieldMatch(newValue) && methodMatch(oldValue, newValue)
        }
    }
}