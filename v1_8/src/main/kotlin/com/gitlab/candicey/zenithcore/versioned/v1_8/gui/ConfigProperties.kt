package com.gitlab.candicey.zenithcore.versioned.v1_8.gui

sealed class ConfigProperties {
    class Shadow {
        @Target(AnnotationTarget.FIELD)
        @Retention(AnnotationRetention.RUNTIME)
        annotation class Field(val ownerClass: String, val fieldName: String, val callWhen: CallWhen = CallWhen.BOTH)

        @Target(AnnotationTarget.FIELD)
        @Retention(AnnotationRetention.RUNTIME)
        annotation class Method(val ownerClass: String, val methodName: String, val callWhen: CallWhen = CallWhen.BOTH)

        enum class CallWhen {
            VALUE_CHANGE,
            GUI_CLOSED,
            BOTH,
        }
    }

    @Target(AnnotationTarget.FIELD)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class DisplayText(val text: String)

    @Target(AnnotationTarget.FIELD)
    @Retention(AnnotationRetention.RUNTIME)
    annotation class Type(val configOptionType: String)

    class Button {
        @Target(AnnotationTarget.FIELD)
        @Retention(AnnotationRetention.RUNTIME)
        annotation class ButtonText(val text: String)

        /**
         * Calls a method on a class with its instance ([ButtonOption.Boolean][com.gitlab.candicey.zenithcore.config.gui.type.ButtonOption.Boolean], [TextOption.Integer][com.gitlab.candicey.zenithcore.config.gui.type.TextOption.Integer], etc.) as a parameter.
         */
        @Target(AnnotationTarget.FIELD)
        @Retention(AnnotationRetention.RUNTIME)
        annotation class ClickHandler(val ownerClass: String, val methodName: String)

        class Boolean {
            class ReflectionMatcher {
                /**
                 * Calls a method on a class that returns a [Boolean] with (newValue: Boolean) parameters.
                 * If the method returns true, the character will be added to the text field.
                 */
                @Target(AnnotationTarget.FIELD)
                @Retention(AnnotationRetention.RUNTIME)
                annotation class Method(val ownerClass: String, val methodName: String)
            }
        }
    }

    class Text {
        @Target(AnnotationTarget.FIELD)
        @Retention(AnnotationRetention.RUNTIME)
        annotation class MaxLength(val maxLength: Int)

        class ReflectionMatcher {
            /**
             * Gets the [Regex] from a field on a class.
             * If the regex matches the text field, the character will be added to the text field.
             */
            @Target(AnnotationTarget.FIELD)
            @Retention(AnnotationRetention.RUNTIME)
            annotation class Field(val ownerClass: String, val fieldName: String)

            /**
             * Calls a method on a class that returns a [Boolean] with (oldValue: String, newValue: String) parameters.
             * If the method returns true, the character will be added to the text field.
             */
            @Target(AnnotationTarget.FIELD)
            @Retention(AnnotationRetention.RUNTIME)
            annotation class Method(val ownerClass: String, val methodName: String)
        }

        class Number {
            @Target(AnnotationTarget.FIELD)
            @Retention(AnnotationRetention.RUNTIME)
            annotation class Min(val min: Float)

            @Target(AnnotationTarget.FIELD)
            @Retention(AnnotationRetention.RUNTIME)
            annotation class Max(val max: Float)
        }
    }
}