package com.gitlab.candicey.zenithcore.versioned.v1_8.gui.type

import com.gitlab.candicey.zenithcore.util.GREEN
import com.gitlab.candicey.zenithcore.util.RED
import com.gitlab.candicey.zenithcore.versioned.v1_8.gameWidth
import com.gitlab.candicey.zenithcore.versioned.v1_8.gui.ConfigOption
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import net.minecraft.client.gui.GuiButton

open class ButtonOption(
    var displayText: String,
    var buttonText: String,
    var onButtonClick: ((ButtonOption) -> Unit)? = null,
    var onGuiClosed: ((ButtonOption) -> Unit)? = null
) : ConfigOption() {
    lateinit var button: GuiButton

    override fun initGui() {
        button = GuiButton(
            -1,                            // id
            gameWidth - 200 - marginRight, // xPosition
            -1,                            // yPosition
            200,                           // width
            20,                            // height
            buttonText                     // displayString
        )
    }

    override fun onGuiClosed() {
        super.onGuiClosed()

        onGuiClosed?.invoke(this)
    }

    override fun onRender() {
        renderText(displayText)

        button.yPosition = startY
        button.drawButton(mc, mouseX, mouseY)
    }

    override fun onClick(buttonCode: Int) {
        if (button.run { mouseX in xPosition..xPosition + width && mouseY in yPosition..yPosition + height }) {
            onButtonClick()
            onButtonClick?.invoke(this)
        }
    }

    protected open fun onButtonClick() {
    }

    companion object {
        const val TYPE = "Button"
    }

    /**
     * @property allowValueChange Called when the button is clicked to check if the value can be changed. (Parameter -> currentValue: Boolean) (Return -> Boolean)
     * @property onValueChange Called when the value of the button is changed. (Parameter -> newValue: Boolean)
     * @property getButtonText Called when the value of the button is changed to get the new button text. (Parameter -> newValue: Boolean) (Return -> String)
     */
    class Boolean(
        displayText: String,
        var value: kotlin.Boolean,
        var onGuiClosedBoolean: ((Boolean) -> Unit)? = null,
        var allowValueChange: (kotlin.Boolean) -> kotlin.Boolean = { true },
        var onValueChange: ((kotlin.Boolean) -> Unit)? = null,
        var getButtonText: (kotlin.Boolean) -> String = { if (it) "${GREEN}Enabled" else "${RED}Disabled" }
    ) : ButtonOption(displayText, getButtonText(value)) {
        override fun onGuiClosed() {
            super.onGuiClosed()

            onGuiClosedBoolean?.invoke(this)
        }

        override fun onButtonClick() {
            if (allowValueChange(value)) {
                value = !value

                button.displayString = getButtonText(value)

                onValueChange?.invoke(value)
            }
        }

        companion object {
            const val TYPE = "Button.Boolean"
        }
    }
}