package com.gitlab.candicey.zenithcore.versioned.v1_8.gui.type

import com.gitlab.candicey.zenithcore.extension.toFloatDefaultIfBlankOrNull
import com.gitlab.candicey.zenithcore.extension.toFloatIfBlankNullOrThrow
import com.gitlab.candicey.zenithcore.extension.toIntDefaultIfBlankOrNull
import com.gitlab.candicey.zenithcore.extension.toIntIfBlankNullOrThrow
import com.gitlab.candicey.zenithcore.versioned.v1_8.child.GuiTextFieldChild
import com.gitlab.candicey.zenithcore.versioned.v1_8.fontRenderer
import com.gitlab.candicey.zenithcore.versioned.v1_8.gameWidth
import com.gitlab.candicey.zenithcore.versioned.v1_8.gui.ConfigOption

open class TextOption(
    var displayText: String,
    var initialButtonText: String? = null,
    var onGuiClosed: ((TextOption) -> Unit)? = null,
    var allowTextChange: (String, String) -> Boolean = { _, _ -> true },
    var onTextChange: ((String, String) -> Unit)? = null
) : ConfigOption() {
    lateinit var textField: GuiTextFieldChild

    override fun initGui() {
        // width = 198 + 2 (border) = 200
        textField = GuiTextFieldChild(
            -1,                            // id
            fontRenderer,                  // fontRenderer
            gameWidth - 199 - marginRight, // xPosition
            -1,                            // yPosition
            198,                           // width
            20                             // height
        ).apply {
            initialButtonText?.let { text = it }
        }
    }

    override fun onGuiClosed() {
        super.onGuiClosed()

        onGuiClosed?.invoke(this)
    }

    override fun onRender() {
        renderText(displayText)

        textField.yPosition = startY
        textField.drawTextBox()
    }

    override fun onClick(buttonCode: Int) {
        textField.mouseClicked(mouseX, mouseY, buttonCode)
    }

    override fun onKeyTyped(char: Char, keyCode: Int) {
        val oldText = textField.text
        val oldCursorPos = textField.cursorPosition
        if (!textField.textboxKeyTyped(char, keyCode)) {
            return
        }
        val newText = textField.text
        val newCursorPos = textField.cursorPosition

        if (oldText == newText) {
            return
        }

        textField.text = oldText
        textField.cursorPosition = oldCursorPos

        if (allowTextChange(oldText, newText) && allowTextChange.invoke(oldText, newText)) {
            textField.text = newText
            textField.cursorPosition = newCursorPos

            if (oldText != textField.text) {
                onTextChange(oldText, textField.text)
                onTextChange?.invoke(oldText, textField.text)
            }
        }
    }

    protected open fun allowTextChange(oldText: String, newText: String): Boolean = true

    protected open fun onTextChange(oldText: String, newText: String) {
    }

    companion object {
        const val TYPE = "Text"
    }

    class Integer(
        displayText: String,
        var initialValue: Int,
        var onGuiClosedInteger: ((Integer) -> Unit)? = null,
        var allowValueChange: (Int, Int) -> Boolean = { _, _ -> true },
        var onValueChange: ((Int?, Int?) -> Unit)? = null
    ) : TextOption(displayText, initialValue.toString()) {
        override fun initGui() {
            super.initGui()

            textField.allowedCharactersMatcher = { char, _ -> char.isDigit() || char == '-' }
        }

        override fun onGuiClosed() {
            super.onGuiClosed()

            onGuiClosedInteger?.invoke(this)
        }

        override fun onTextChange(oldText: String, newText: String) {
            onValueChange?.let { func ->
                val oldValue = if (oldText.startsWith('-')) null else oldText.toIntIfBlankNullOrThrow()
                val newValue = if (newText == "-") null else newText.toIntIfBlankNullOrThrow()

                if (oldValue != newValue) {
                    func(oldValue, newValue)
                }
            }
        }

        override fun allowTextChange(oldText: String, newText: String): Boolean {
            val oldValue = oldText.toIntDefaultIfBlankOrNull(initialValue) ?: if (oldText.startsWith('-')) initialValue else return false
            val newValue = newText.toIntDefaultIfBlankOrNull(initialValue) ?: if (newText == "-") initialValue else return false

            return allowValueChange(oldValue, newValue)
        }

        companion object {
            const val TYPE = "Text.Integer"
        }
    }

    class Float(
        displayText: String,
        var initialValue: kotlin.Float,
        var onGuiClosedFloat: ((Float) -> Unit)? = null,
        var allowValueChange: (kotlin.Float, kotlin.Float) -> Boolean = { _, _ -> true },
        var onValueChange: ((kotlin.Float?, kotlin.Float?) -> Unit)? = null
    ) : TextOption(displayText, initialValue.toString()) {
        override fun initGui() {
            super.initGui()

            textField.allowedCharactersMatcher = { char, _ -> char.isDigit() || char == '-' || char == '.' }
        }

        override fun onGuiClosed() {
            super.onGuiClosed()

            onGuiClosedFloat?.invoke(this)
        }

        override fun onTextChange(oldText: String, newText: String) {
            onValueChange?.let { func ->
                val oldFloat = if (oldText.startsWith('-')) null else oldText.toFloatIfBlankNullOrThrow()
                val newFloat = if (newText == "-") null else newText.toFloatIfBlankNullOrThrow()

                if (oldFloat != newFloat) {
                    func(oldFloat, newFloat)
                }
            }
        }

        override fun allowTextChange(oldText: String, newText: String): Boolean {
            val oldValue = oldText.toFloatDefaultIfBlankOrNull(initialValue) ?: if (oldText.startsWith('-')) initialValue else return false
            val newValue = newText.toFloatDefaultIfBlankOrNull(initialValue) ?: if (newText == "-") initialValue else return false

            return allowValueChange(oldValue, newValue)
        }

        companion object {
            const val TYPE = "Text.Float"
        }
    }
}