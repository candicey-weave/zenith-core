package com.gitlab.candicey.zenithcore.versioned.v1_8.helper

import com.gitlab.candicey.zenithcore.extension.subscribeEventListener
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.TickEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import net.minecraft.client.gui.GuiScreen
import net.weavemc.api.event.SubscribeEvent

/**
 * This class is used to open a GUI screen from a non-main thread to avoid crashes.
 */
object GuiScreenHelper {
    private var guiScreen: GuiScreen? = null

    init {
        subscribeEventListener()
    }

    /**
     * Adds a GUI screen to the queue to be opened.
     */
    fun addGuiOpenQueue(guiScreen: GuiScreen) {
        GuiScreenHelper.guiScreen = guiScreen
    }

    @SubscribeEvent
    fun onTickEvent(event: TickEvent.Post) {
        if (guiScreen != null) {
            mc.displayGuiScreen(guiScreen)
            guiScreen = null
        }
    }
}