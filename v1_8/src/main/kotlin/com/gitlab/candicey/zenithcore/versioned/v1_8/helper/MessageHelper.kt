package com.gitlab.candicey.zenithcore.versioned.v1_8.helper

import com.gitlab.candicey.zenithcore.versioned.v1_8.event.ChatReceivedEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.ChatSentEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.ignoreMessageHook
import net.weavemc.api.event.SubscribeEvent
import java.util.function.Consumer

/**
 * A helper class for message events.
 *
 * @see [ChatReceivedEvent]
 * @see [ChatSentEvent]
 */
object MessageHelper {
    private var s2cConsumer: MutableList<Consumer<ChatReceivedEvent>> = mutableListOf()
        @Synchronized get
        @Synchronized set
    private var c2sConsumer: MutableList<Consumer<ChatSentEvent>> = mutableListOf()
        @Synchronized get
        @Synchronized set

    /**
     * Adds a consumer to the list of consumers that will be called when a [ChatReceivedEvent] is fired.
     *
     * @param consumer The consumer to add.
     */
    fun addS2C(consumer: Consumer<ChatReceivedEvent>) = s2cConsumer.add(consumer)


    /**
     * Adds a consumer to the list of consumers that will be called when a [ChatSentEvent] is fired.
     *
     * @param consumer The consumer to add.
     */
    fun addC2S(consumer: Consumer<ChatSentEvent>) = c2sConsumer.add(consumer)


    /**
     * Removes a consumer from the list of consumers that will be called when a [ChatReceivedEvent] is fired.
     *
     * @param consumer The consumer to remove.
     */
    fun removeS2C(consumer: Consumer<ChatReceivedEvent>) = s2cConsumer.remove(consumer)


    /**
     * Removes a consumer from the list of consumers that will be called when a [ChatSentEvent] is fired.
     *
     * @param consumer The consumer to remove.
     */
    fun removeC2S(consumer: Consumer<ChatSentEvent>) = c2sConsumer.remove(consumer)

    @SubscribeEvent
    fun s2c(message: ChatReceivedEvent) {
        if (message.message.ignoreMessageHook == true) {
            return
        }

        s2cConsumer.toMutableList().forEach { it.accept(message) }
    }

    @SubscribeEvent
    fun c2s(message: ChatSentEvent) = c2sConsumer.toMutableList().forEach { it.accept(message) }
}