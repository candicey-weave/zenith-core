package com.gitlab.candicey.zenithcore.versioned.v1_8.helper

import com.gitlab.candicey.zenithcore.versioned.v1_8.event.TickEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.helper.ScaledResolutionHelper.scaledResolution
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import net.minecraft.client.gui.ScaledResolution
import net.weavemc.api.event.SubscribeEvent

/**
 * Updates the [scaledResolution] every tick.
 */
object ScaledResolutionHelper {
    lateinit var scaledResolution: ScaledResolution

    @SubscribeEvent
    fun onTick(event: TickEvent.Pre) {
        scaledResolution = ScaledResolution(mc)
    }
}