package com.gitlab.candicey.zenithcore.versioned.v1_8.helper

import com.gitlab.candicey.zenithcore.util.reflectMethod
import com.gitlab.candicey.zenithcore.util.runAsync
import com.gitlab.candicey.zenithcore.versioned.v1_8.gameHeight
import com.gitlab.candicey.zenithcore.versioned.v1_8.gameWidth
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import net.minecraft.client.renderer.GlStateManager
import net.minecraft.client.renderer.OpenGlHelper
import net.minecraft.client.renderer.texture.TextureUtil
import net.minecraft.util.ScreenShotHelper
import org.lwjgl.BufferUtils
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL12
import java.awt.image.BufferedImage
import java.io.File
import java.nio.IntBuffer
import javax.imageio.ImageIO
import kotlin.properties.Delegates

/**
 * A helper class for taking screenshots.
 *
 * @see ScreenShotHelper
 */
object ScreenshotHelper {
    private val getTimestampedPNGFileForDirectoryMethod = reflectMethod<ScreenShotHelper>("getTimestampedPNGFileForDirectory", File::class.java)

    private var pixelBuffer: IntBuffer? = null
    private var pixelValues: IntArray? = null

    /**
     * Generates a timestamped PNG file in the screenshots directory.
     *
     * @return The [File] object of the screenshot which is yet to be created.
     */
    fun generateTimestampedPngFile(): File {
        val screenshotsFolder = File(mc.mcDataDir, "screenshots")
        if (!screenshotsFolder.exists()) {
            screenshotsFolder.mkdirs()
        }

        return getTimestampedPNGFileForDirectoryMethod.invoke(null, screenshotsFolder) as File
    }

    /**
     * Saves the screenshot to the specified file asynchronously so as not to affect the game.
     *
     * @param file The file to save the screenshot to.
     * @param callBack The callback function, the first parameter is whether the screenshot is saved successfully, the second parameter is the file, the third parameter is the exception.
     */
    fun saveScreenshotToFile(file: File = generateTimestampedPngFile(), callBack: (Boolean, File?, Throwable?) -> Unit) {
        runCatching {
            val frameBuffer = mc.framebuffer
            val framebufferEnabled = OpenGlHelper.isFramebufferEnabled()

            var width by Delegates.notNull<Int>()
            var height by Delegates.notNull<Int>()

            if (framebufferEnabled) {
                width = mc.displayWidth
                height = mc.displayHeight
            } else {
                width = gameWidth
                height = gameHeight
            }

            val pixels = width * height
            if (pixelBuffer == null || pixelBuffer!!.capacity() < pixels) {
                pixelBuffer = BufferUtils.createIntBuffer(pixels)
                pixelValues = IntArray(pixels)
            }

            GL11.glPixelStorei(GL11.GL_PACK_ALIGNMENT, 1)
            GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1)
            pixelBuffer!!.clear()

            if (framebufferEnabled) {
                GlStateManager.bindTexture(frameBuffer.framebufferTexture)
                GL11.glGetTexImage(GL11.GL_TEXTURE_2D, 0, GL12.GL_BGRA, GL12.GL_UNSIGNED_INT_8_8_8_8_REV, pixelBuffer)
            } else {
                GL11.glReadPixels(0, 0, width, height, GL12.GL_BGRA, GL12.GL_UNSIGNED_INT_8_8_8_8_REV, pixelBuffer)
            }

            pixelBuffer!!.get(pixelValues)

            runAsync {
                TextureUtil.processPixelValues(pixelValues, width, height)
                val bufferedImage: BufferedImage?

                if (framebufferEnabled) {
                    bufferedImage = BufferedImage(frameBuffer.framebufferWidth, frameBuffer.framebufferHeight, 1)
                    val i = frameBuffer.framebufferTextureHeight - frameBuffer.framebufferHeight

                    for (j in i until frameBuffer.framebufferTextureHeight) {
                        for (k in 0 until frameBuffer.framebufferWidth) {
                            bufferedImage.setRGB(k, j - i, pixelValues!![j * frameBuffer.framebufferTextureWidth + k])
                        }
                    }
                } else {
                    bufferedImage = BufferedImage(width, height, 1)
                    bufferedImage.setRGB(0, 0, width, height, pixelValues, 0, width)
                }

                ImageIO.write(bufferedImage, "png", file)

                callBack(true, file, null)
            }
        }.onFailure { callBack(false, null, it) }
    }
}