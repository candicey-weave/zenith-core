package com.gitlab.candicey.zenithcore.versioned.v1_8.hook

import com.gitlab.candicey.zenithcore.extension.addFieldSafe
import net.weavemc.api.Hook
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode

/**
 * Adds a boolean field called `ignoreMessageHook` to [net.minecraft.util.ChatComponentText] that is used to determine whether the [com.gitlab.candicey.zenithcore.helper.MessageHelper] should ignore the message.
 */
object ChatComponentTextHook : Hook("net/minecraft/util/ChatComponentText") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.addFieldSafe(
            FieldNode(
                Opcodes.ACC_PUBLIC,
                "ignoreMessageHook",
                "Z",
                null,
                false
            )
        )
    }
}