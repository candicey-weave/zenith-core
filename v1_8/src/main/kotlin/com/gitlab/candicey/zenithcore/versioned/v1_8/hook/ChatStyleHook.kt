package com.gitlab.candicey.zenithcore.versioned.v1_8.hook

import com.gitlab.candicey.zenithcore.extension.addFieldSafe
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.zenithClickEvent
import net.minecraft.util.ChatStyle
import net.weavemc.api.Hook
import net.weavemc.internals.asm
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode
import org.objectweb.asm.tree.JumpInsnNode

/**
 * Adds a [ChatStyle.zenithClickEvent] field to [ChatStyle], adds hooks to [ChatStyle.createShallowCopy] and [ChatStyle.createDeepCopy] to copy the [ChatStyle.zenithClickEvent] field, and adds a hook to [ChatStyle.isEmpty] to check if the [ChatStyle.zenithClickEvent] field is null.
 *
 * @see [GuiScreenHook]
 */
object ChatStyleHook : Hook("net/minecraft/util/ChatStyle") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.addFieldSafe(
            FieldNode(
                Opcodes.ACC_PUBLIC,
                "zenithClickEvent",
                "Lnet/minecraft/event/ClickEvent;",
                null,
                null
            )
        )

        val methods = node.methods

        val createShallowCopyMethodInstructions = methods.named("createShallowCopy").instructions
        createShallowCopyMethodInstructions.insert(
            createShallowCopyMethodInstructions.find { it.opcode == Opcodes.ARETURN }!!.previous,
            asm {
                aload(0)
                aload(1)
                invokestatic(
                    internalNameOf<ChatStyleHook>(),
                    "onCreateShallowCopy",
                    "(Lnet/minecraft/util/ChatStyle;Lnet/minecraft/util/ChatStyle;)V",
                )
            })

        val createDeepCopyMethodInstructions = methods.named("createDeepCopy").instructions
        createDeepCopyMethodInstructions.insert(
            createDeepCopyMethodInstructions.find { it.opcode == Opcodes.ARETURN }!!.previous,
            asm {
                aload(0)
                aload(1)
                invokestatic(
                    internalNameOf<ChatStyleHook>(),
                    "onCreateDeepCopy",
                    "(Lnet/minecraft/util/ChatStyle;Lnet/minecraft/util/ChatStyle;)V",
                )
            })

        val isEmptyMethodInstructions = methods.named("isEmpty").instructions
        val jumpInsnNode = isEmptyMethodInstructions.find { it.opcode == Opcodes.IFNONNULL } as JumpInsnNode
        isEmptyMethodInstructions.insert(asm {
            aload(0)
            getfield("net/minecraft/util/ChatStyle", "zenithClickEvent", "Lnet/minecraft/event/ClickEvent;")
            ifnonnull(jumpInsnNode.label)
        })
    }

    @JvmStatic
    fun onCreateShallowCopy(chatStyle: ChatStyle, newChatStyle: ChatStyle) {
        newChatStyle.zenithClickEvent = chatStyle.zenithClickEvent
    }

    @JvmStatic
    fun onCreateDeepCopy(chatStyle: ChatStyle, newChatStyle: ChatStyle) {
        newChatStyle.zenithClickEvent = chatStyle.zenithClickEvent
    }
}