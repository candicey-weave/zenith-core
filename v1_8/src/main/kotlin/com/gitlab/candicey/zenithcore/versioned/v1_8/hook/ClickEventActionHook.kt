package com.gitlab.candicey.zenithcore.versioned.v1_8.hook

import com.gitlab.candicey.zenithcore.extension.addFieldSafe
import com.gitlab.candicey.zenithcore.extension.value
import com.gitlab.candicey.zenithcore.util.weave.named
import com.gitlab.candicey.zenithcore.versioned.v1_8.warn
import net.weavemc.api.Hook
import net.weavemc.internals.asm
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode
import org.objectweb.asm.tree.IntInsnNode

/**
 * Adds new values to the [net.minecraft.event.ClickEvent.Action] enum.
 *
 * @see [GuiScreenHook]
 * @see [ClickEventHook]
 * @see [com.gitlab.candicey.zenithcore.versioned.v1_8.event.CustomClickEvent]
 */
object ClickEventActionHook : Hook("net/minecraft/event/ClickEvent\$Action") {
    /**
     * The data to add to the enum. The first value is the name of the enum field, the second value is the value of the enum field.
     */
    val injectingData = mutableListOf(
        "NONE" to arrayOf("none", false),
        "COPY_TEXT_TO_CLIPBOARD" to arrayOf("copy_text_to_clipboard", false),
        "COPY_IMAGE_TO_CLIPBOARD" to arrayOf("copy_image_to_clipboard", false),
    )

    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.run {
            if (access and Opcodes.ACC_ENUM == 0) {
                throw IllegalArgumentException("Class is not an enum!")
            }

            val initMethodDescriptor = methods.named("<init>").desc

            val clinitInsnList = methods.named("<clinit>").instructions
            val biPushInsnNode = clinitInsnList.find { it.opcode == Opcodes.BIPUSH }
                ?: throw IllegalStateException("Could not find BIPUSH in <clinit> method!")
            val intInsnNode = biPushInsnNode as IntInsnNode
            intInsnNode.operand += injectingData.size

            val aNewArrayInsnNode = clinitInsnList.find { it.opcode == Opcodes.ANEWARRAY }
                ?: throw IllegalStateException("Could not find ANEWARRAY in <clinit> method!")

            var enumFieldSize = fields.filter { it.desc == "L$name;" }.size

            for ((enumFieldName, value) in injectingData) {
                if (fields.map(FieldNode::name).contains(enumFieldName)) {
                    warn("Field with name $enumFieldName already exists in class ${name}.")
                    continue
                }

                val fieldNode = FieldNode(
                    Opcodes.ACC_PUBLIC or Opcodes.ACC_STATIC or Opcodes.ACC_FINAL or Opcodes.ACC_ENUM,
                    enumFieldName,
                    "L$name;",
                    null,
                    null
                )

                node.addFieldSafe(fieldNode)

                clinitInsnList.insertBefore(biPushInsnNode, asm {
                    new(name)
                    dup
                    ldc(enumFieldName)
                    value(enumFieldSize)
                    value.forEach(::value)
                    invokespecial(name, "<init>", initMethodDescriptor)
                    putstatic(name, enumFieldName, "L$name;")
                })

                clinitInsnList.insert(aNewArrayInsnNode, asm {
                    dup
                    value(enumFieldSize)
                    getstatic(name, enumFieldName, "L$name;")
                    aastore
                })

                enumFieldSize++
            }
        }
    }
}