package com.gitlab.candicey.zenithcore.versioned.v1_8.hook

import com.gitlab.candicey.zenithcore.extension.addFieldSafe
import com.gitlab.candicey.zenithcore.extension.descriptor
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.zenithRunnable
import net.minecraft.event.ClickEvent
import net.weavemc.api.Hook
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode
import org.objectweb.asm.tree.FieldNode

/**
 * Adds a field to the ClickEvent class that allows for the execution of a Runnable.
 *
 * The runnable is intended to be used with [com.gitlab.candicey.zenithcore.versioned.v1_8.event.CustomClickEvent.NONE].
 *
 * @see [ClickEventActionHook]
 * @see [GuiScreenHook]
 */
object ClickEventHook : Hook("net/minecraft/event/ClickEvent") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.addFieldSafe(
            FieldNode(
                Opcodes.ACC_PUBLIC,
                ClickEvent::zenithRunnable.name,
                Runnable::class.java.descriptor,
                null,
                null
            )
        )
    }
}