package com.gitlab.candicey.zenithcore.versioned.v1_8.hook

import com.gitlab.candicey.zenithcore.util.callStaticsBooleanIf
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.cancel
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.networkManager
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.previousGuiScreen
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.toChatComponent
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import net.minecraft.client.gui.GuiButton
import net.minecraft.client.multiplayer.GuiConnecting
import net.weavemc.api.Hook
import org.objectweb.asm.tree.ClassNode

object GuiConnectingHook : Hook("net/minecraft/client/multiplayer/GuiConnecting") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.callStaticsBooleanIf<GuiConnectingHook>(
            "actionPerformed",
        )

        cfg.computeFrames()
    }

    /**
     * Lunar Client modifies the [GuiConnecting.actionPerformed] method to sometimes not close the gui when the cancel button is pressed.
     */
    @JvmStatic
    fun onActionPerformed(guiConnecting: GuiConnecting, button: GuiButton): Boolean {
        if (button.id == 0) {
            guiConnecting.cancel = true
            guiConnecting.networkManager?.closeChannel("Aborted".toChatComponent())
            mc.displayGuiScreen(guiConnecting.previousGuiScreen)

            return true
        }

        return false
    }
}