package com.gitlab.candicey.zenithcore.versioned.v1_8.hook

import com.gitlab.candicey.zenithcore.helper.ClipboardHelper
import com.gitlab.candicey.zenithcore.util.callStatics
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.CustomClickEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.zenithClickEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.zenithRunnable
import com.gitlab.candicey.zenithcore.versioned.v1_8.warn
import net.minecraft.client.gui.GuiScreen
import net.minecraft.util.IChatComponent
import net.weavemc.api.Hook
import org.objectweb.asm.tree.ClassNode
import java.io.File
import javax.imageio.ImageIO

/**
 * Adds support for custom click events and runnables when clicking on an [IChatComponent] in the chat.
 *
 * @see [com.gitlab.candicey.zenithcore.versioned.v1_8.event.CustomClickEvent]
 * @see [ClickEventHook]
 * @see [ClickEventActionHook]
 */
object GuiScreenHook : Hook("net/minecraft/client/gui/GuiScreen") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.callStatics<GuiScreenHook>(
            "handleComponentClick",
        )
    }

    @JvmStatic
    fun onHandleComponentClick(guiScreen: GuiScreen, iChatComponent: IChatComponent?) {
        val zenithClickEvent = iChatComponent?.chatStyle?.zenithClickEvent ?: return

        if (GuiScreen.isShiftKeyDown()) {
            return
        }

        when (zenithClickEvent.action ?: return) {
            CustomClickEvent.COPY_TEXT_TO_CLIPBOARD -> {
                val value = zenithClickEvent.value ?: return

                ClipboardHelper.setClipboardString(value)
            }

            CustomClickEvent.COPY_IMAGE_TO_CLIPBOARD -> {
                val value = zenithClickEvent.value ?: return

                val file = File(value)
                if (!file.exists()) {
                    warn("File not found: ${file.canonicalFile}")
                    return
                }
                if (file.isDirectory) {
                    warn("File is a directory: ${file.canonicalFile}")
                    return
                }

                ClipboardHelper.setClipboardImage(ImageIO.read(file))
            }

            else -> {
                // Do nothing
            }
        }

        zenithClickEvent.zenithRunnable?.run()
    }
}