package com.gitlab.candicey.zenithcore.versioned.v1_8.hook

import com.gitlab.candicey.zenithcore.extension.descriptor
import com.gitlab.candicey.zenithcore.util.callStaticsNotNull
import net.minecraft.client.resources.Locale
import net.weavemc.api.Hook
import org.objectweb.asm.tree.ClassNode

object LocaleHook : Hook("net/minecraft/client/resources/Locale") {
    /**
     * A map of unlocalised strings to their localised counterparts.
     *
     * @see [com.gitlab.candicey.zenithcore.manager.KeyBindManager]
     */
    val messages: MutableMap<String, String> = mutableMapOf(
        // "zenithcore.keybind.test" to "Test KeyBind"
    )

    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.callStaticsNotNull<LocaleHook>(
            "formatMessage" to String::class.java.descriptor
        )
    }

    @JvmStatic
    fun onFormatMessage(locale: Locale, string: String, objects: Array<Any>): String? = messages[string]
}