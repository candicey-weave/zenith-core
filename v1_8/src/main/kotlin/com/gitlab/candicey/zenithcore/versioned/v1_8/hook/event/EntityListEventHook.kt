package com.gitlab.candicey.zenithcore.versioned.v1_8.hook.event

import com.gitlab.candicey.zenithcore.util.weave.callEvent
import com.gitlab.candicey.zenithcore.util.weave.internalNameOf
import com.gitlab.candicey.zenithcore.util.weave.named
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.EntityListEvent
import net.weavemc.api.Hook
import net.weavemc.internals.asm
import org.objectweb.asm.Opcodes
import org.objectweb.asm.tree.ClassNode

internal object EntityListEventAddHook : Hook("net/minecraft/world/World") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        node.methods.named("spawnEntityInWorld").instructions.insert(asm {
            new(internalNameOf<EntityListEvent.Add>())
            dup
            aload(1)
            invokespecial(
                internalNameOf<EntityListEvent.Add>(),
                "<init>",
                "(Lnet/minecraft/entity/Entity;)V"
            )
            callEvent()
        })
    }
}

internal object EntityListEventRemoveHook : Hook("net/minecraft/client/multiplayer/WorldClient") {
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val mn = node.methods.named("removeEntityFromWorld")
        mn.instructions.insert(mn.instructions.find { it.opcode == Opcodes.IFNULL }, asm {
            new(internalNameOf<EntityListEvent.Remove>())
            dup
            aload(2)
            invokespecial(
                internalNameOf<EntityListEvent.Remove>(),
                "<init>",
                "(Lnet/minecraft/entity/Entity;)V"
            )
            callEvent()
        })
    }
}
