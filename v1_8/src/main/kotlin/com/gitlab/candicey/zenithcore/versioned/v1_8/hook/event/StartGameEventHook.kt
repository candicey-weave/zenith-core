package com.gitlab.candicey.zenithcore.versioned.v1_8.hook.event

import com.gitlab.candicey.zenithcore.util.weave.callEvent
import com.gitlab.candicey.zenithcore.util.weave.getSingleton
import com.gitlab.candicey.zenithcore.util.weave.named
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.StartGameEvent
import net.weavemc.api.Hook
import net.weavemc.internals.asm
import org.objectweb.asm.Opcodes.RETURN
import org.objectweb.asm.tree.ClassNode

/**
 * Corresponds to [StartGameEvent.Pre] and [StartGameEvent.Post].
 */
internal object StartGameEventHook : Hook("net/minecraft/client/Minecraft") {
    /**
     * Inserts a call to [net.minecraft.client.Minecraft.startGame] using the Event Bus.
     *
     * [StartGameEvent.Pre] is called at the head of [net.minecraft.client.Minecraft.startGame]. Whereas
     * [StartGameEvent.Post] is called at the tail of [net.minecraft.client.Minecraft.startGame].
     *
     * @see net.minecraft.client.Minecraft.startGame
     */
    override fun transform(node: ClassNode, cfg: AssemblerConfig) {
        val mn = node.methods.named("startGame")

        mn.instructions.insert(asm {
            getSingleton<StartGameEvent.Pre>()
            callEvent()
        })

        mn.instructions.insertBefore(mn.instructions.findLast { it.opcode == RETURN }, asm {
            getSingleton<StartGameEvent.Post>()
            callEvent()
        })
    }
}
