@file:Suppress("MemberVisibilityCanBePrivate")

package com.gitlab.candicey.zenithcore.versioned.v1_8.keybind

import com.gitlab.candicey.zenithcore.versioned.v1_8.event.KeyboardEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.event.LateKeyboardEvent
import com.gitlab.candicey.zenithcore.versioned.v1_8.extension.addKeyBindings
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import net.minecraft.client.settings.KeyBinding
import net.weavemc.api.event.SubscribeEvent

/**
 * Manages custom key bindings.
 *
 * @see [com.gitlab.candicey.zenithcore.versioned.v1_8.hook.LocaleHook]
 */
object KeyBindManager {
    /**
     * Key bindings that are triggered before the game processes the key press.
     */
    private val keyBindings: MutableList<KeyBind> = mutableListOf()

    /**
     * Key bindings that are triggered after the game processes the key press.
     */
    private val lateKeyBindings: MutableList<KeyBind> = mutableListOf()

    private fun Array<out KeyBind>.register() {
        mc.gameSettings.addKeyBindings(*this)
        mc.gameSettings.loadOptions()
    }

    /**
     * Example:
     * ```kotlin
     * addKeyBindings(KeyBind("zenithcore.keybind.test", Keyboard.KEY_0, "Zenith Core") { _, _ -> println("test") })
     * ```
     */
    fun registerKeyBindings(vararg keyBindings: KeyBind) {
        this.keyBindings.addAll(keyBindings)
        keyBindings.register()
    }

    /**
     * Example:
     * ```kotlin
     * addLateKeyBindings(KeyBind("zenithcore.keybind.test", Keyboard.KEY_0, "Zenith Core") { _, _ -> println("test") })
     * ```
     */
    fun registerLateKeyBindings(vararg keyBindings: KeyBind) {
        this.lateKeyBindings.addAll(keyBindings)
        keyBindings.register()
    }

    /**
     * @return A cloned list of key bindings.
     */
    fun getKeyBindings(): List<KeyBind> = keyBindings.toList()

    /**
     * @return A cloned list of late key bindings.
     */
    fun getLateKeyBindings(): List<KeyBind> = lateKeyBindings.toList()

    @SubscribeEvent
    fun onKeyboardEvent(event: KeyboardEvent) {
        keyBindings
            .filterNot { it.onlyActivateWhenKeyPressed && !it.isPressed }
            .forEach { it.onEvent?.invoke(it, event) }
    }

    @SubscribeEvent
    fun onLateKeyboardEvent(event: LateKeyboardEvent) {
        lateKeyBindings
            .filterNot { it.onlyActivateWhenKeyPressed && !it.isPressed }
            .forEach { it.onEvent?.invoke(it, KeyboardEvent()) }
    }
}

/**
 * @property name The non-localised name of the key binding.
 * @property keyCode The key code of the key binding. Use [org.lwjgl.input.Keyboard] for key codes.
 * @property category The non-localised category of the key binding.
 * @property onlyActivateWhenKeyPressed Whether the [onEvent] should only be called when the specified key is pressed. If false, the [onEvent] will be called when any key is pressed.
 * @property onEvent The callback that is called when the key binding is triggered.
 */
class KeyBind(
    val name: String,
    @get:JvmName("getKeyBindCode")
    val keyCode: Int,
    val category: String,
    val onlyActivateWhenKeyPressed: Boolean = true,
    val onEvent: ((KeyBind, KeyboardEvent) -> Unit)? = null
) : KeyBinding(
    name,
    keyCode,
    category,
)