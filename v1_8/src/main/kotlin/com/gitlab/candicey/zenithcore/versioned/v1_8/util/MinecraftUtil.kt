package com.gitlab.candicey.zenithcore.versioned.v1_8.util

import com.gitlab.candicey.zenithcore.nameRegex
import com.gitlab.candicey.zenithcore.versioned.v1_8.mc
import com.mojang.authlib.GameProfile

object MinecraftUtil {
    /**
     * Gets all online players from the [net.minecraft.client.network.NetHandlerPlayClient.playerInfoMap].
     *
     * @param checkValidName Whether to check if the player's name is valid or not using [com.gitlab.candicey.zenithcore.nameRegex].
     * @return A list of [GameProfile]s.
     */
    fun getOnlinePlayers(checkValidName: Boolean = true): List<GameProfile> =
        mc.thePlayer.sendQueue.playerInfoMap.map { it.gameProfile }.filter { !checkValidName || nameRegex.matches(it.name) }
}